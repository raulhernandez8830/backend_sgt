package com.edesal.sgt.dao;

import com.edesal.sgt.models.DetalleManiobra;
import com.edesal.sgt.models.Maniobra;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by DHernandez on 3/9/2019.
 */
public interface DetalleManiobraDAO extends CrudRepository<DetalleManiobra, Long> {

    public DetalleManiobra findTopById(Long id);
    public int countByManiobra(Maniobra m);
    public int countByManiobraAndAsociada(Maniobra m, Integer asoc);
}
