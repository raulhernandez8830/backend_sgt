package com.edesal.sgt.dao;

import com.edesal.sgt.models.Corte;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by DHernandez on 4/9/2019.
 */
public interface CorteDAO extends CrudRepository<Corte,Integer> {
}
