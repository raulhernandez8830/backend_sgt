package com.edesal.sgt.dao;

import com.edesal.sgt.models.DetalleCT;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by DHernandez on 3/6/2020.
 */
public interface DetalleCTDAO extends CrudRepository<DetalleCT, Long> {
}
