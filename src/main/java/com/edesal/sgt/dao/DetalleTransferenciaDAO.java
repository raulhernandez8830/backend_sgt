package com.edesal.sgt.dao;

import com.edesal.sgt.models.DetalleTransferencia;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by DHernandez on 5/9/2019.
 */
public interface DetalleTransferenciaDAO extends CrudRepository<DetalleTransferencia, Integer> {
}
