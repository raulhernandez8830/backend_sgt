package com.edesal.sgt.dao;

import com.edesal.sgt.models.Energizado;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by DHernandez on 7/10/2019.
 */
public interface EnergizadoDAO extends CrudRepository<Energizado, Long> {
}
