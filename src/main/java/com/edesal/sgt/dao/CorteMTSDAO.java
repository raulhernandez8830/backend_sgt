package com.edesal.sgt.dao;

import com.edesal.sgt.models.CorteMTS;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by DHernandez on 1/10/2019.
 */
public interface CorteMTSDAO extends CrudRepository<CorteMTS, Long> {
}
