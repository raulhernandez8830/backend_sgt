package com.edesal.sgt.dao;

import com.edesal.sgt.models.Usuario;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by DHernandez on 19/8/2019.
 */
public interface UsuarioDAO extends CrudRepository<Usuario,Integer>{
}
