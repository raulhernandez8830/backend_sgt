package com.edesal.sgt.dao;

import com.edesal.sgt.models.DetalleInterrupcion;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by DHernandez on 12/11/2019.
 */
public interface DetalleInterrupcionDAO extends CrudRepository<DetalleInterrupcion,Long > {
}
