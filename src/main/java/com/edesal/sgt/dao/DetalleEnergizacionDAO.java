package com.edesal.sgt.dao;

import com.edesal.sgt.models.DetallesEnergizacion;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by DHernandez on 18/10/2019.
 */
public interface DetalleEnergizacionDAO extends CrudRepository<DetallesEnergizacion, Long> {
}
