package com.edesal.sgt.dao;

import com.edesal.sgt.models.Transformador;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by DHernandez on 15/5/2020.
 */
public interface TransformadorDAO extends CrudRepository<Transformador, Long> {


}
