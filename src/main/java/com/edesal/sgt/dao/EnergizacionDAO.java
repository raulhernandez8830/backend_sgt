package com.edesal.sgt.dao;

import com.edesal.sgt.models.Energizacion;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by DHernandez on 15/10/2019.
 */
public interface EnergizacionDAO extends CrudRepository<Energizacion, Long> {
}
