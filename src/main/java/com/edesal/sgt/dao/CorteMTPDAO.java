package com.edesal.sgt.dao;

import com.edesal.sgt.models.CorteMTP;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by DHernandez on 1/10/2019.
 */
public interface CorteMTPDAO extends CrudRepository<CorteMTP, Long> {
}
