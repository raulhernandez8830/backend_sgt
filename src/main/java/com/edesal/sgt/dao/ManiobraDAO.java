package com.edesal.sgt.dao;

import com.edesal.sgt.models.Maniobra;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by DHernandez on 23/8/2019.
 */
public interface ManiobraDAO extends JpaRepository<Maniobra,Long> {



    Maniobra getOne(Long id);
    Maniobra getManiobraById(Long id);
}


