package com.edesal.sgt.dao;

import com.edesal.sgt.models.Interrupcion;
import com.edesal.sgt.models.UtilidadConteoENS;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

/**
 * Created by DHernandez on 21/8/2019.
 */
public interface InterrupcionDAO extends CrudRepository<Interrupcion,Long> {

   // obtener la ultima interrupcion almacenada en la db
    public Interrupcion findTopByOrderByIdDesc();

    // obtener interrupcion por su id
    public Interrupcion findTopById(Long id);




}
