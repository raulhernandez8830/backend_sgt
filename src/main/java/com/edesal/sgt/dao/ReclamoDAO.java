package com.edesal.sgt.dao;

import com.edesal.sgt.models.Reclamo;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by DHernandez on 17/9/2019.
 */
public interface ReclamoDAO extends CrudRepository<Reclamo, String> {

    // obtener un reclamo por su id
    public Reclamo findTopByCorrelativo(String correlativo);
}
