package com.edesal.sgt.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

/**
 * Created by DHernandez on 2/6/2020.
 */
@Entity
@Table(name = "SGT_ct_detalles")
public class DetalleCT {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private String marca;
    private String num_serie;
    private Double kva;
    private String calibre_fusible;
    private String posicion_tap;
    private String cod_material;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "trafo_id")
    @JsonIgnore
    private Transformador transformador;


    public Transformador getTransformador() {
        return transformador;
    }

    public void setTransformador(Transformador transformador) {
        this.transformador = transformador;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getNum_serie() {
        return num_serie;
    }

    public void setNum_serie(String num_serie) {
        this.num_serie = num_serie;
    }

    public Double getKva() {
        return kva;
    }

    public void setKva(Double kva) {
        this.kva = kva;
    }

    public String getCalibre_fusible() {
        return calibre_fusible;
    }

    public void setCalibre_fusible(String calibre_fusible) {
        this.calibre_fusible = calibre_fusible;
    }

    public String getPosicion_tap() {
        return posicion_tap;
    }

    public void setPosicion_tap(String posicion_tap) {
        this.posicion_tap = posicion_tap;
    }

    public String getCod_material() {
        return cod_material;
    }

    public void setCod_material(String cod_material) {
        this.cod_material = cod_material;
    }


}

