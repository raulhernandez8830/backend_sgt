package com.my.db;

public class Act_postepozo {
  private String id_pos;
  private String cod_material;
  private Long pospoz;
  private String propiedad;
  private String tipo_vano;
  private Long num_ckts;
  private Double coord_x;
  private Double coord_y;
  private String cod_depto;
  private String cod_municipio;
  private String area_tendido;
  private String impuesto_uso_suelo;
  private java.sql.Timestamp fecha_alta_sistema;
  private String usu_alta_sistema;
  private java.sql.Timestamp fecha_mod_sistema;
  private String usu_mod_sistema;
  private java.sql.Timestamp fecha_alta_vnr;
  private java.sql.Timestamp fecha_baja_vnr;
  private String motivo_baja_vnr;
  private String ubicacion;
  private String cod_proyecto;
  private String cod_cuadricula;

  public String getId_pos() {
    return id_pos;
  }

  public void setId_pos(String id_pos) {
    this.id_pos = id_pos;
  }

  public String getCod_material() {
    return cod_material;
  }

  public void setCod_material(String cod_material) {
    this.cod_material = cod_material;
  }

  public Long getPospoz() {
    return pospoz;
  }

  public void setPospoz(Long pospoz) {
    this.pospoz = pospoz;
  }

  public String getPropiedad() {
    return propiedad;
  }

  public void setPropiedad(String propiedad) {
    this.propiedad = propiedad;
  }

  public String getTipo_vano() {
    return tipo_vano;
  }

  public void setTipo_vano(String tipo_vano) {
    this.tipo_vano = tipo_vano;
  }

  public Long getNum_ckts() {
    return num_ckts;
  }

  public void setNum_ckts(Long num_ckts) {
    this.num_ckts = num_ckts;
  }

  public Double getCoord_x() {
    return coord_x;
  }

  public void setCoord_x(Double coord_x) {
    this.coord_x = coord_x;
  }

  public Double getCoord_y() {
    return coord_y;
  }

  public void setCoord_y(Double coord_y) {
    this.coord_y = coord_y;
  }

  public String getCod_depto() {
    return cod_depto;
  }

  public void setCod_depto(String cod_depto) {
    this.cod_depto = cod_depto;
  }

  public String getCod_municipio() {
    return cod_municipio;
  }

  public void setCod_municipio(String cod_municipio) {
    this.cod_municipio = cod_municipio;
  }

  public String getArea_tendido() {
    return area_tendido;
  }

  public void setArea_tendido(String area_tendido) {
    this.area_tendido = area_tendido;
  }

  public String getImpuesto_uso_suelo() {
    return impuesto_uso_suelo;
  }

  public void setImpuesto_uso_suelo(String impuesto_uso_suelo) {
    this.impuesto_uso_suelo = impuesto_uso_suelo;
  }

  public java.sql.Timestamp getFecha_alta_sistema() {
    return fecha_alta_sistema;
  }

  public void setFecha_alta_sistema(java.sql.Timestamp fecha_alta_sistema) {
    this.fecha_alta_sistema = fecha_alta_sistema;
  }

  public String getUsu_alta_sistema() {
    return usu_alta_sistema;
  }

  public void setUsu_alta_sistema(String usu_alta_sistema) {
    this.usu_alta_sistema = usu_alta_sistema;
  }

  public java.sql.Timestamp getFecha_mod_sistema() {
    return fecha_mod_sistema;
  }

  public void setFecha_mod_sistema(java.sql.Timestamp fecha_mod_sistema) {
    this.fecha_mod_sistema = fecha_mod_sistema;
  }

  public String getUsu_mod_sistema() {
    return usu_mod_sistema;
  }

  public void setUsu_mod_sistema(String usu_mod_sistema) {
    this.usu_mod_sistema = usu_mod_sistema;
  }

  public java.sql.Timestamp getFecha_alta_vnr() {
    return fecha_alta_vnr;
  }

  public void setFecha_alta_vnr(java.sql.Timestamp fecha_alta_vnr) {
    this.fecha_alta_vnr = fecha_alta_vnr;
  }

  public java.sql.Timestamp getFecha_baja_vnr() {
    return fecha_baja_vnr;
  }

  public void setFecha_baja_vnr(java.sql.Timestamp fecha_baja_vnr) {
    this.fecha_baja_vnr = fecha_baja_vnr;
  }

  public String getMotivo_baja_vnr() {
    return motivo_baja_vnr;
  }

  public void setMotivo_baja_vnr(String motivo_baja_vnr) {
    this.motivo_baja_vnr = motivo_baja_vnr;
  }

  public String getUbicacion() {
    return ubicacion;
  }

  public void setUbicacion(String ubicacion) {
    this.ubicacion = ubicacion;
  }

  public String getCod_proyecto() {
    return cod_proyecto;
  }

  public void setCod_proyecto(String cod_proyecto) {
    this.cod_proyecto = cod_proyecto;
  }

  public String getCod_cuadricula() {
    return cod_cuadricula;
  }

  public void setCod_cuadricula(String cod_cuadricula) {
    this.cod_cuadricula = cod_cuadricula;
  }
}
