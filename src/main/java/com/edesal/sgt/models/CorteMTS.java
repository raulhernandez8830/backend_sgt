package com.edesal.sgt.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by DHernandez on 1/10/2019.
 */
@Entity
@Table(name = "SGT_corte_mt_secundario")
public class CorteMTS {

    @Id
    private String id;
    private String elemento_aperturado;

    private String cierre_secundario;

    public CorteMTS() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getElemento_aperturado() {
        return elemento_aperturado;
    }

    public void setElemento_aperturado(String elemento_aperturado) {
        this.elemento_aperturado = elemento_aperturado;
    }



    public String getCierre_secundario() {
        return cierre_secundario;
    }

    public void setCierre_secundario(String cierre_secundario) {
        this.cierre_secundario = cierre_secundario;
    }
}
