package com.edesal.sgt.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by DHernandez on 6/3/2020.
 */
@Entity
@Table(name = "SGT_interrupcionesSIGET")
public class InterrupcionSIGET {
    @Id
    private Long id;
    private String idinter;
    private String sistema;
    private String origen;
    private String tipo;
    private String fechain;
    private String divired;
    private String idelem;
    private String tipoelem;
    private String ssee;
    private String circuito;


    public InterrupcionSIGET() {
    }

    public InterrupcionSIGET(String idinter, String sistema, String origen, String tipo, String fechain, String divired, String idelem, String tipoelem, String ssee, String circuito) {
        this.idinter = idinter;
        this.sistema = sistema;
        this.origen = origen;
        this.tipo = tipo;
        this.fechain = fechain;
        this.divired = divired;
        this.idelem = idelem;
        this.tipoelem = tipoelem;
        this.ssee = ssee;
        this.circuito = circuito;
    }



    public String getIdinter() {
        return idinter;
    }

    public void setIdinter(String idinter) {
        this.idinter = idinter;
    }

    public String getSistema() {
        return sistema;
    }

    public void setSistema(String sistema) {
        this.sistema = sistema;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getFechain() {
        return fechain;
    }

    public void setFechain(String fechain) {
        this.fechain = fechain;
    }

    public String getDivired() {
        return divired;
    }

    public void setDivired(String divired) {
        this.divired = divired;
    }

    public String getIdelem() {
        return idelem;
    }

    public void setIdelem(String idelem) {
        this.idelem = idelem;
    }

    public String getTipoelem() {
        return tipoelem;
    }

    public void setTipoelem(String tipoelem) {
        this.tipoelem = tipoelem;
    }

    public String getSsee() {
        return ssee;
    }

    public void setSsee(String ssee) {
        this.ssee = ssee;
    }

    public String getCircuito() {
        return circuito;
    }

    public void setCircuito(String circuito) {
        this.circuito = circuito;
    }
}
