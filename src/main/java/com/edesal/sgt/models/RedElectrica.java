package com.edesal.sgt.models;

/**
 * Created by DHernandez on 4/9/2019.
 */
public class RedElectrica {

    private String padre;
    private String hijo;

    public RedElectrica(String padre, String hijo) {
        this.padre = padre;
        this.hijo = hijo;
    }

    public String getPadre() {
        return padre;
    }


    public RedElectrica() {

    }

    public void setPadre(String padre) {
        this.padre = padre;
    }

    public String getHijo() {
        return hijo;
    }

    public void setHijo(String hijo) {
        this.hijo = hijo;
    }
}
