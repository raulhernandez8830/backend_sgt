package com.edesal.sgt.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Id;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by DHernandez on 5/9/2019.
 */
@Entity
@Table(name = "SGT_detalle_transferencia")
public class DetalleTransferencia {

    @javax.persistence.Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String corte_hijo;
    private String corte_padre;
    private String fecha_inicio;
    private String fecha_fin;



    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_transferencia")
    @JsonIgnore
    private Transferencia transferencia;

    public DetalleTransferencia() {
    }

    public DetalleTransferencia( String corte_hijo, String corte_padre, String fecha_inicio, String fecha_fin,  Transferencia transferencia) {

        this.corte_hijo = corte_hijo;
        this.corte_padre = corte_padre;
        this.fecha_inicio = fecha_inicio;
        this.fecha_fin = fecha_fin;
        this.transferencia = transferencia;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



    public String getCorte_hijo() {
        return corte_hijo;
    }

    public void setCorte_hijo(String corte_hijo) {
        this.corte_hijo = corte_hijo;
    }

    public String getCorte_padre() {
        return corte_padre;
    }

    public void setCorte_padre(String corte_padre) {
        this.corte_padre = corte_padre;
    }

    public String getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(String fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public String getFecha_fin() {
        return fecha_fin;
    }

    public void setFecha_fin(String fecha_fin) {
        this.fecha_fin = fecha_fin;
    }



    public Transferencia getTransferencia() {
        return transferencia;
    }

    public void setTransferencia(Transferencia transferencia) {
        this.transferencia = transferencia;
    }
}
