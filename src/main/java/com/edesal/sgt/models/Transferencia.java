package com.edesal.sgt.models;

/**
 * Created by DHernandez on 5/9/2019.
 */

import net.bytebuddy.dynamic.loading.ClassReloadingStrategy;
import org.hibernate.annotations.GeneratorType;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name="SGT_transferencias")
public class Transferencia {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Integer usuario;
    private String fecha_creacion;
    private String estado;
    private Long tipo_transferencia;

    @OneToMany(mappedBy = "transferencia", cascade = CascadeType.ALL)
    private List<DetalleTransferencia> detallestransferencia;


    public Transferencia(Long tipo_transferencia,Integer usuario, String fecha_creacion, String estado, List<DetalleTransferencia> detallestransferencia) {
        this.usuario = usuario;
        this.fecha_creacion = fecha_creacion;
        this.estado = estado;
        this.detallestransferencia = detallestransferencia;
        this.tipo_transferencia = tipo_transferencia;
    }

    public Transferencia() {

    }

    public List<DetalleTransferencia> getDetallestransferencia() {
        return detallestransferencia;
    }

    public void setDetallestransferencia(List<DetalleTransferencia> detallestransferencia) {
        this.detallestransferencia = detallestransferencia;
    }

    public Long getTipo_transferencia() {
        return tipo_transferencia;
    }

    public void setTipo_transferencia(Long tipo_transferencia) {
        this.tipo_transferencia = tipo_transferencia;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getUsuario() {
        return usuario;
    }

    public void setUsuario(Integer usuario) {
        this.usuario = usuario;
    }

    public String getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
