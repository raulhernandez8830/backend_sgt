package com.edesal.sgt.models;

/**
 * Created by DHernandez on 20/5/2020.
 */
public class ElementoPoste {
    String elemento;

    public String getPoste() {
        return elemento;
    }

    public void setPoste(String elemento) {
        this.elemento = elemento;
    }
}
