package com.edesal.sgt.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by DHernandez on 17/9/2019.
 */
@Entity
@Table(name = "fe_calidad_reclamos")
public class Reclamo {

    @Id
    private String correlativo;
    private String periodo;
    private java.sql.Timestamp fecha;
    private String codigo_usuario;
    private String codigo_sucursal;
    private String codigo_tipo_rreclamo;
    private String codigo_mreclamo;
    private String nombres_reporta;
    private String apellidos_reporta;
    private String nombres;
    private String apellidos;
    private String telefono;
    private String poligono;
    private String casa;
    private String num_suministro;
    private String codigo_tarifa;
    private String numero_medidor;
    private String codigo_elemento;
    private String codigo_elemento_padre;
    private String comentario;
    private String codigo_departamento;
    private String codigo_municipio;
    private String codigo_poblacion;
    private String codigo_colonia;
    private String calificacion_reclamo;
    private String codigo_justificacion;
    private String codigo_reclamo;
    private String prioridad;
    private String codigo_tipo_ordentrabajo;
    private String usuario_orden_inspeccion;
    private String numero_orden_inspeccion;
    private java.sql.Timestamp fecha_orden_inspeccion;
    private String tiempo_orden_inspeccion;
    private java.sql.Timestamp fecha_resolucion_orden;
    private String tiempo_resolucion_orden;
    private String comentario_orden_inspeccion;
    private String usuario_orden_apoyo;
    private java.sql.Timestamp fecha_usuario_apoyo;
    private String reclamo_cerrado;
    private String servicio_nuevo;
    private String modificacion_red;
    private String codigo_naturaleza;
    private String codigo_tipo_servicio;
    private String codigo_area_geografica;
    private String periodo_cierre;
    private String reconexion;
    private String consulta;
    private String codigo_mconsulta;
    private String reclamo_principal;
    private String codigo_reclamo_padre;
    private String codigo_msuspension;
    private java.sql.Timestamp fecha_respuesta;
    private java.sql.Timestamp fecha_cierre;
    private String usuario_cierre;
    private String numero_factura;
    private String monto_reclamo;
    private String trabajos_pendientes;
    private String respuesta_otorgada;
    private Long alerta;
    private String resultado;
    private String usuario_apertura;
    private java.sql.Timestamp fecha_apertura;
    private String motivo_apertura;
    private String t_normado;
    private String t_interno;
    private String t_unidad;
    private String usu_impresion;
    private java.sql.Timestamp fec_impresion;
    private String lectura;
    private String sistema;
    private String contacto;




    public Interrupcion getInterrupcion() {
        return interrupcion;
    }

    public void setInterrupcion(Interrupcion interrupcion) {
        this.interrupcion = interrupcion;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "interrupcion_asociada")
    @JsonIgnore
    private Interrupcion interrupcion;

    public Reclamo(Interrupcion interrupcion,String correlativo, String periodo, Timestamp fecha, String codigo_usuario, String codigo_sucursal, String codigo_tipo_rreclamo, String codigo_mreclamo, String nombres_reporta, String apellidos_reporta, String nombres, String apellidos, String telefono, String poligono, String casa, String num_suministro, String codigo_tarifa, String numero_medidor, String codigo_elemento, String codigo_elemento_padre, String comentario, String codigo_departamento, String codigo_municipio, String codigo_poblacion, String codigo_colonia, String calificacion_reclamo, String codigo_justificacion, String codigo_reclamo, String prioridad, String codigo_tipo_ordentrabajo, String usuario_orden_inspeccion, String numero_orden_inspeccion, Timestamp fecha_orden_inspeccion, String tiempo_orden_inspeccion, Timestamp fecha_resolucion_orden, String tiempo_resolucion_orden, String comentario_orden_inspeccion, String usuario_orden_apoyo, Timestamp fecha_usuario_apoyo, String reclamo_cerrado, String servicio_nuevo, String modificacion_red, String codigo_naturaleza, String codigo_tipo_servicio, String codigo_area_geografica, String periodo_cierre, String reconexion, String consulta, String codigo_mconsulta, String reclamo_principal, String codigo_reclamo_padre, String codigo_msuspension, Timestamp fecha_respuesta, Timestamp fecha_cierre, String usuario_cierre, String numero_factura, String monto_reclamo, String trabajos_pendientes, String respuesta_otorgada, Long alerta, String resultado, String usuario_apertura, Timestamp fecha_apertura, String motivo_apertura, String t_normado, String t_interno, String t_unidad, String usu_impresion, Timestamp fec_impresion, String lectura, String sistema, String contacto) {
        this.correlativo = correlativo;
        this.periodo = periodo;
        this.fecha = fecha;
        this.codigo_usuario = codigo_usuario;
        this.codigo_sucursal = codigo_sucursal;
        this.codigo_tipo_rreclamo = codigo_tipo_rreclamo;
        this.codigo_mreclamo = codigo_mreclamo;
        this.nombres_reporta = nombres_reporta;
        this.apellidos_reporta = apellidos_reporta;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.telefono = telefono;
        this.poligono = poligono;
        this.casa = casa;
        this.num_suministro = num_suministro;
        this.codigo_tarifa = codigo_tarifa;
        this.numero_medidor = numero_medidor;
        this.codigo_elemento = codigo_elemento;
        this.codigo_elemento_padre = codigo_elemento_padre;
        this.comentario = comentario;
        this.codigo_departamento = codigo_departamento;
        this.codigo_municipio = codigo_municipio;
        this.codigo_poblacion = codigo_poblacion;
        this.codigo_colonia = codigo_colonia;
        this.calificacion_reclamo = calificacion_reclamo;
        this.codigo_justificacion = codigo_justificacion;
        this.codigo_reclamo = codigo_reclamo;
        this.prioridad = prioridad;
        this.codigo_tipo_ordentrabajo = codigo_tipo_ordentrabajo;
        this.usuario_orden_inspeccion = usuario_orden_inspeccion;
        this.numero_orden_inspeccion = numero_orden_inspeccion;
        this.fecha_orden_inspeccion = fecha_orden_inspeccion;
        this.tiempo_orden_inspeccion = tiempo_orden_inspeccion;
        this.fecha_resolucion_orden = fecha_resolucion_orden;
        this.tiempo_resolucion_orden = tiempo_resolucion_orden;
        this.comentario_orden_inspeccion = comentario_orden_inspeccion;
        this.usuario_orden_apoyo = usuario_orden_apoyo;
        this.fecha_usuario_apoyo = fecha_usuario_apoyo;
        this.reclamo_cerrado = reclamo_cerrado;
        this.servicio_nuevo = servicio_nuevo;
        this.modificacion_red = modificacion_red;
        this.codigo_naturaleza = codigo_naturaleza;
        this.codigo_tipo_servicio = codigo_tipo_servicio;
        this.codigo_area_geografica = codigo_area_geografica;
        this.periodo_cierre = periodo_cierre;
        this.reconexion = reconexion;
        this.consulta = consulta;
        this.codigo_mconsulta = codigo_mconsulta;
        this.reclamo_principal = reclamo_principal;
        this.codigo_reclamo_padre = codigo_reclamo_padre;
        this.codigo_msuspension = codigo_msuspension;
        this.fecha_respuesta = fecha_respuesta;
        this.fecha_cierre = fecha_cierre;
        this.usuario_cierre = usuario_cierre;
        this.numero_factura = numero_factura;
        this.monto_reclamo = monto_reclamo;
        this.trabajos_pendientes = trabajos_pendientes;
        this.respuesta_otorgada = respuesta_otorgada;
        this.alerta = alerta;
        this.resultado = resultado;
        this.usuario_apertura = usuario_apertura;
        this.fecha_apertura = fecha_apertura;
        this.motivo_apertura = motivo_apertura;
        this.t_normado = t_normado;
        this.t_interno = t_interno;
        this.t_unidad = t_unidad;
        this.usu_impresion = usu_impresion;
        this.fec_impresion = fec_impresion;
        this.lectura = lectura;
        this.sistema = sistema;
        this.contacto = contacto;
        this.interrupcion = interrupcion;
    }

    public Reclamo() {
    }

    public String getCorrelativo() {
        return correlativo;
    }

    public void setCorrelativo(String correlativo) {
        this.correlativo = correlativo;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public java.sql.Timestamp getFecha() {
        return fecha;
    }

    public void setFecha(java.sql.Timestamp fecha) {
        this.fecha = fecha;
    }

    public String getCodigo_usuario() {
        return codigo_usuario;
    }

    public void setCodigo_usuario(String codigo_usuario) {
        this.codigo_usuario = codigo_usuario;
    }

    public String getCodigo_sucursal() {
        return codigo_sucursal;
    }

    public void setCodigo_sucursal(String codigo_sucursal) {
        this.codigo_sucursal = codigo_sucursal;
    }

    public String getCodigo_tipo_rreclamo() {
        return codigo_tipo_rreclamo;
    }

    public void setCodigo_tipo_rreclamo(String codigo_tipo_rreclamo) {
        this.codigo_tipo_rreclamo = codigo_tipo_rreclamo;
    }

    public String getCodigo_mreclamo() {
        return codigo_mreclamo;
    }

    public void setCodigo_mreclamo(String codigo_mreclamo) {
        this.codigo_mreclamo = codigo_mreclamo;
    }

    public String getNombres_reporta() {
        return nombres_reporta;
    }

    public void setNombres_reporta(String nombres_reporta) {
        this.nombres_reporta = nombres_reporta;
    }

    public String getApellidos_reporta() {
        return apellidos_reporta;
    }

    public void setApellidos_reporta(String apellidos_reporta) {
        this.apellidos_reporta = apellidos_reporta;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getPoligono() {
        return poligono;
    }

    public void setPoligono(String poligono) {
        this.poligono = poligono;
    }

    public String getCasa() {
        return casa;
    }

    public void setCasa(String casa) {
        this.casa = casa;
    }

    public String getNum_suministro() {
        return num_suministro;
    }

    public void setNum_suministro(String num_suministro) {
        this.num_suministro = num_suministro;
    }

    public String getCodigo_tarifa() {
        return codigo_tarifa;
    }

    public void setCodigo_tarifa(String codigo_tarifa) {
        this.codigo_tarifa = codigo_tarifa;
    }

    public String getNumero_medidor() {
        return numero_medidor;
    }

    public void setNumero_medidor(String numero_medidor) {
        this.numero_medidor = numero_medidor;
    }

    public String getCodigo_elemento() {
        return codigo_elemento;
    }

    public void setCodigo_elemento(String codigo_elemento) {
        this.codigo_elemento = codigo_elemento;
    }

    public String getCodigo_elemento_padre() {
        return codigo_elemento_padre;
    }

    public void setCodigo_elemento_padre(String codigo_elemento_padre) {
        this.codigo_elemento_padre = codigo_elemento_padre;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getCodigo_departamento() {
        return codigo_departamento;
    }

    public void setCodigo_departamento(String codigo_departamento) {
        this.codigo_departamento = codigo_departamento;
    }

    public String getCodigo_municipio() {
        return codigo_municipio;
    }

    public void setCodigo_municipio(String codigo_municipio) {
        this.codigo_municipio = codigo_municipio;
    }

    public String getCodigo_poblacion() {
        return codigo_poblacion;
    }

    public void setCodigo_poblacion(String codigo_poblacion) {
        this.codigo_poblacion = codigo_poblacion;
    }

    public String getCodigo_colonia() {
        return codigo_colonia;
    }

    public void setCodigo_colonia(String codigo_colonia) {
        this.codigo_colonia = codigo_colonia;
    }

    public String getCalificacion_reclamo() {
        return calificacion_reclamo;
    }

    public void setCalificacion_reclamo(String calificacion_reclamo) {
        this.calificacion_reclamo = calificacion_reclamo;
    }

    public String getCodigo_justificacion() {
        return codigo_justificacion;
    }

    public void setCodigo_justificacion(String codigo_justificacion) {
        this.codigo_justificacion = codigo_justificacion;
    }

    public String getCodigo_reclamo() {
        return codigo_reclamo;
    }

    public void setCodigo_reclamo(String codigo_reclamo) {
        this.codigo_reclamo = codigo_reclamo;
    }

    public String getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(String prioridad) {
        this.prioridad = prioridad;
    }

    public String getCodigo_tipo_ordentrabajo() {
        return codigo_tipo_ordentrabajo;
    }

    public void setCodigo_tipo_ordentrabajo(String codigo_tipo_ordentrabajo) {
        this.codigo_tipo_ordentrabajo = codigo_tipo_ordentrabajo;
    }

    public String getUsuario_orden_inspeccion() {
        return usuario_orden_inspeccion;
    }

    public void setUsuario_orden_inspeccion(String usuario_orden_inspeccion) {
        this.usuario_orden_inspeccion = usuario_orden_inspeccion;
    }

    public String getNumero_orden_inspeccion() {
        return numero_orden_inspeccion;
    }

    public void setNumero_orden_inspeccion(String numero_orden_inspeccion) {
        this.numero_orden_inspeccion = numero_orden_inspeccion;
    }

    public java.sql.Timestamp getFecha_orden_inspeccion() {
        return fecha_orden_inspeccion;
    }

    public void setFecha_orden_inspeccion(java.sql.Timestamp fecha_orden_inspeccion) {
        this.fecha_orden_inspeccion = fecha_orden_inspeccion;
    }

    public String getTiempo_orden_inspeccion() {
        return tiempo_orden_inspeccion;
    }

    public void setTiempo_orden_inspeccion(String tiempo_orden_inspeccion) {
        this.tiempo_orden_inspeccion = tiempo_orden_inspeccion;
    }

    public java.sql.Timestamp getFecha_resolucion_orden() {
        return fecha_resolucion_orden;
    }

    public void setFecha_resolucion_orden(java.sql.Timestamp fecha_resolucion_orden) {
        this.fecha_resolucion_orden = fecha_resolucion_orden;
    }

    public String getTiempo_resolucion_orden() {
        return tiempo_resolucion_orden;
    }

    public void setTiempo_resolucion_orden(String tiempo_resolucion_orden) {
        this.tiempo_resolucion_orden = tiempo_resolucion_orden;
    }

    public String getComentario_orden_inspeccion() {
        return comentario_orden_inspeccion;
    }

    public void setComentario_orden_inspeccion(String comentario_orden_inspeccion) {
        this.comentario_orden_inspeccion = comentario_orden_inspeccion;
    }

    public String getUsuario_orden_apoyo() {
        return usuario_orden_apoyo;
    }

    public void setUsuario_orden_apoyo(String usuario_orden_apoyo) {
        this.usuario_orden_apoyo = usuario_orden_apoyo;
    }

    public java.sql.Timestamp getFecha_usuario_apoyo() {
        return fecha_usuario_apoyo;
    }

    public void setFecha_usuario_apoyo(java.sql.Timestamp fecha_usuario_apoyo) {
        this.fecha_usuario_apoyo = fecha_usuario_apoyo;
    }

    public String getReclamo_cerrado() {
        return reclamo_cerrado;
    }

    public void setReclamo_cerrado(String reclamo_cerrado) {
        this.reclamo_cerrado = reclamo_cerrado;
    }

    public String getServicio_nuevo() {
        return servicio_nuevo;
    }

    public void setServicio_nuevo(String servicio_nuevo) {
        this.servicio_nuevo = servicio_nuevo;
    }

    public String getModificacion_red() {
        return modificacion_red;
    }

    public void setModificacion_red(String modificacion_red) {
        this.modificacion_red = modificacion_red;
    }

    public String getCodigo_naturaleza() {
        return codigo_naturaleza;
    }

    public void setCodigo_naturaleza(String codigo_naturaleza) {
        this.codigo_naturaleza = codigo_naturaleza;
    }

    public String getCodigo_tipo_servicio() {
        return codigo_tipo_servicio;
    }

    public void setCodigo_tipo_servicio(String codigo_tipo_servicio) {
        this.codigo_tipo_servicio = codigo_tipo_servicio;
    }

    public String getCodigo_area_geografica() {
        return codigo_area_geografica;
    }

    public void setCodigo_area_geografica(String codigo_area_geografica) {
        this.codigo_area_geografica = codigo_area_geografica;
    }

    public String getPeriodo_cierre() {
        return periodo_cierre;
    }

    public void setPeriodo_cierre(String periodo_cierre) {
        this.periodo_cierre = periodo_cierre;
    }

    public String getReconexion() {
        return reconexion;
    }

    public void setReconexion(String reconexion) {
        this.reconexion = reconexion;
    }

    public String getConsulta() {
        return consulta;
    }

    public void setConsulta(String consulta) {
        this.consulta = consulta;
    }

    public String getCodigo_mconsulta() {
        return codigo_mconsulta;
    }

    public void setCodigo_mconsulta(String codigo_mconsulta) {
        this.codigo_mconsulta = codigo_mconsulta;
    }

    public String getReclamo_principal() {
        return reclamo_principal;
    }

    public void setReclamo_principal(String reclamo_principal) {
        this.reclamo_principal = reclamo_principal;
    }

    public String getCodigo_reclamo_padre() {
        return codigo_reclamo_padre;
    }

    public void setCodigo_reclamo_padre(String codigo_reclamo_padre) {
        this.codigo_reclamo_padre = codigo_reclamo_padre;
    }

    public String getCodigo_msuspension() {
        return codigo_msuspension;
    }

    public void setCodigo_msuspension(String codigo_msuspension) {
        this.codigo_msuspension = codigo_msuspension;
    }

    public java.sql.Timestamp getFecha_respuesta() {
        return fecha_respuesta;
    }

    public void setFecha_respuesta(java.sql.Timestamp fecha_respuesta) {
        this.fecha_respuesta = fecha_respuesta;
    }

    public java.sql.Timestamp getFecha_cierre() {
        return fecha_cierre;
    }

    public void setFecha_cierre(java.sql.Timestamp fecha_cierre) {
        this.fecha_cierre = fecha_cierre;
    }

    public String getUsuario_cierre() {
        return usuario_cierre;
    }

    public void setUsuario_cierre(String usuario_cierre) {
        this.usuario_cierre = usuario_cierre;
    }

    public String getNumero_factura() {
        return numero_factura;
    }

    public void setNumero_factura(String numero_factura) {
        this.numero_factura = numero_factura;
    }

    public String getMonto_reclamo() {
        return monto_reclamo;
    }

    public void setMonto_reclamo(String monto_reclamo) {
        this.monto_reclamo = monto_reclamo;
    }

    public String getTrabajos_pendientes() {
        return trabajos_pendientes;
    }

    public void setTrabajos_pendientes(String trabajos_pendientes) {
        this.trabajos_pendientes = trabajos_pendientes;
    }

    public String getRespuesta_otorgada() {
        return respuesta_otorgada;
    }

    public void setRespuesta_otorgada(String respuesta_otorgada) {
        this.respuesta_otorgada = respuesta_otorgada;
    }

    public Long getAlerta() {
        return alerta;
    }

    public void setAlerta(Long alerta) {
        this.alerta = alerta;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public String getUsuario_apertura() {
        return usuario_apertura;
    }

    public void setUsuario_apertura(String usuario_apertura) {
        this.usuario_apertura = usuario_apertura;
    }

    public java.sql.Timestamp getFecha_apertura() {
        return fecha_apertura;
    }

    public void setFecha_apertura(java.sql.Timestamp fecha_apertura) {
        this.fecha_apertura = fecha_apertura;
    }

    public String getMotivo_apertura() {
        return motivo_apertura;
    }

    public void setMotivo_apertura(String motivo_apertura) {
        this.motivo_apertura = motivo_apertura;
    }

    public String getT_normado() {
        return t_normado;
    }

    public void setT_normado(String t_normado) {
        this.t_normado = t_normado;
    }

    public String getT_interno() {
        return t_interno;
    }

    public void setT_interno(String t_interno) {
        this.t_interno = t_interno;
    }

    public String getT_unidad() {
        return t_unidad;
    }

    public void setT_unidad(String t_unidad) {
        this.t_unidad = t_unidad;
    }

    public String getUsu_impresion() {
        return usu_impresion;
    }

    public void setUsu_impresion(String usu_impresion) {
        this.usu_impresion = usu_impresion;
    }

    public java.sql.Timestamp getFec_impresion() {
        return fec_impresion;
    }

    public void setFec_impresion(java.sql.Timestamp fec_impresion) {
        this.fec_impresion = fec_impresion;
    }

    public String getLectura() {
        return lectura;
    }

    public void setLectura(String lectura) {
        this.lectura = lectura;
    }

    public String getSistema() {
        return sistema;
    }

    public void setSistema(String sistema) {
        this.sistema = sistema;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

}
