package com.edesal.sgt.models;

import javax.persistence.*;

/**
 * Created by DHernandez on 27/8/2019.
 */
@Entity
@Table(name = "ENS_CAUSA_INTERRUPCION")
public class Causa {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String codigo_causa;
    private String descripcion_causa;

    //constructor
    public Causa() {

    }

    public String getCodigo_causa() {
        return codigo_causa;
    }

    public void setCodigo_causa(String codigo_causa) {
        this.codigo_causa = codigo_causa;
    }

    public String getDescripcion_causa() {
        return descripcion_causa;
    }

    public void setDescripcion_causa(String descripcion_causa) {
        this.descripcion_causa = descripcion_causa;
    }
}
