package com.edesal.sgt.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by DHernandez on 24/5/2020.
 */
@Entity
@Table(name = "ens_materiales_estructura")
public class MaterialesEstructura {
    @Id
    private String cod_mat_est;
    private String des_mat_est;
    private String costo_material;
    private String costo_montaje;

    public String getCod_mat_est() {
        return cod_mat_est;
    }

    public void setCod_mat_est(String cod_mat_est) {
        this.cod_mat_est = cod_mat_est;
    }

    public String getDes_mat_est() {
        return des_mat_est;
    }

    public void setDes_mat_est(String des_mat_est) {
        this.des_mat_est = des_mat_est;
    }

    public String getCosto_material() {
        return costo_material;
    }

    public void setCosto_material(String costo_material) {
        this.costo_material = costo_material;
    }

    public String getCosto_montaje() {
        return costo_montaje;
    }

    public void setCosto_montaje(String costo_montaje) {
        this.costo_montaje = costo_montaje;
    }
}
