package com.edesal.sgt.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by DHernandez on 18/5/2020.
 */
@Entity
@Table(name = "ENS_Circuito")
public class Circuito {
    private String codigo_distribuidora;
    private String codigo_red;
    private String codigo_circuito;
    private String tipo_elemento;
    @Id
    private String sec_circuito;
    private String codigo_nivel;
    private String bandera_activo;
    private String descripcion;
    private String ruta_imagen;
    private String cod_material;
    private String propiedad;
    private String x;
    private String y;
    private String id_salmt;
    private String id_emp;
    private String id_set;
    private String id_ssee;
    private Long nfases;
    private String fase;
    private String neutro;
    private String cod_voltaje;
    private String capacidad;

    public Circuito() {
    }

    public String getCodigo_distribuidora() {
        return codigo_distribuidora;
    }

    public void setCodigo_distribuidora(String codigo_distribuidora) {
        this.codigo_distribuidora = codigo_distribuidora;
    }


    public String getCodigo_red() {
        return codigo_red;
    }

    public void setCodigo_red(String codigo_red) {
        this.codigo_red = codigo_red;
    }

    public String getCodigo_circuito() {
        return codigo_circuito;
    }

    public void setCodigo_circuito(String codigo_circuito) {
        this.codigo_circuito = codigo_circuito;
    }

    public String getTipo_elemento() {
        return tipo_elemento;
    }

    public void setTipo_elemento(String tipo_elemento) {
        this.tipo_elemento = tipo_elemento;
    }

    public String getSec_circuito() {
        return sec_circuito;
    }

    public void setSec_circuito(String sec_circuito) {
        this.sec_circuito = sec_circuito;
    }

    public String getCodigo_nivel() {
        return codigo_nivel;
    }

    public void setCodigo_nivel(String codigo_nivel) {
        this.codigo_nivel = codigo_nivel;
    }

    public String getBandera_activo() {
        return bandera_activo;
    }

    public void setBandera_activo(String bandera_activo) {
        this.bandera_activo = bandera_activo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getRuta_imagen() {
        return ruta_imagen;
    }

    public void setRuta_imagen(String ruta_imagen) {
        this.ruta_imagen = ruta_imagen;
    }

    public String getCod_material() {
        return cod_material;
    }

    public void setCod_material(String cod_material) {
        this.cod_material = cod_material;
    }

    public String getPropiedad() {
        return propiedad;
    }

    public void setPropiedad(String propiedad) {
        this.propiedad = propiedad;
    }

    public String getX() {
        return x;
    }

    public void setX(String x) {
        this.x = x;
    }

    public String getY() {
        return y;
    }

    public void setY(String y) {
        this.y = y;
    }

    public String getId_salmt() {
        return id_salmt;
    }

    public void setId_salmt(String id_salmt) {
        this.id_salmt = id_salmt;
    }

    public String getId_emp() {
        return id_emp;
    }

    public void setId_emp(String id_emp) {
        this.id_emp = id_emp;
    }

    public String getId_set() {
        return id_set;
    }

    public void setId_set(String id_set) {
        this.id_set = id_set;
    }

    public String getId_ssee() {
        return id_ssee;
    }

    public void setId_ssee(String id_ssee) {
        this.id_ssee = id_ssee;
    }

    public Long getNfases() {
        return nfases;
    }

    public void setNfases(Long nfases) {
        this.nfases = nfases;
    }

    public String getFase() {
        return fase;
    }

    public void setFase(String fase) {
        this.fase = fase;
    }

    public String getNeutro() {
        return neutro;
    }

    public void setNeutro(String neutro) {
        this.neutro = neutro;
    }

    public String getCod_voltaje() {
        return cod_voltaje;
    }

    public void setCod_voltaje(String cod_voltaje) {
        this.cod_voltaje = cod_voltaje;
    }

    public String getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(String capacidad) {
        this.capacidad = capacidad;
    }
}
