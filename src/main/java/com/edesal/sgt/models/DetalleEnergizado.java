package com.edesal.sgt.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

/**
 * Created by DHernandez on 7/10/2019.
 */
@Entity
@Table(name = "SGT_dt_energizados")
public class DetalleEnergizado {

    @Id
    private Long id;
    private String corte_energizado;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "configuracion")
    @JsonIgnore
    Energizado energizado;


    public Energizado getEnergizado() {
        return energizado;
    }

    public void setEnergizado(Energizado energizado) {
        this.energizado = energizado;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



    public String getCorte_energizado() {
        return corte_energizado;
    }

    public void setCorte_energizado(String corte_energizado) {
        this.corte_energizado = corte_energizado;
    }
}
