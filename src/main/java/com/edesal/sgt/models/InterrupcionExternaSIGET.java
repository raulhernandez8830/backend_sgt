package com.edesal.sgt.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by DHernandez on 7/3/2020.
 */
@Entity
@Table(name = "SGT_externasSIGET")
public class InterrupcionExternaSIGET {
    @Id
    private Long id;
    private String idinter;
    private Double ens;
    private String fechainext;
    private String fecharpext;





    public String getIdinter() {
        return idinter;
    }

    public void setIdinter(String idinter) {
        this.idinter = idinter;
    }



    public Double getEns() {
        return ens;
    }

    public void setEns(Double ens) {
        this.ens = ens;
    }

    public String getFechainext() {
        return fechainext;
    }

    public void setFechainext(String fechainext) {
        this.fechainext = fechainext;
    }

    public String getFecharpext() {
        return fecharpext;
    }

    public void setFecharpext(String fecharpext) {
        this.fecharpext = fecharpext;
    }
}
