package com.edesal.sgt.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

/**
 * Created by DHernandez on 21/5/2020.
 */
@Entity
@Table(name="ens_voltaje")
public class Voltaje {
    @Id
    private String cod_voltaje;
    private String nom_voltaje;






    public String getCod_voltaje() {
        return cod_voltaje;
    }

    public void setCod_voltaje(String cod_voltaje) {
        this.cod_voltaje = cod_voltaje;
    }

    public String getNom_voltaje() {
        return nom_voltaje;
    }

    public void setNom_voltaje(String nom_voltaje) {
        this.nom_voltaje = nom_voltaje;
    }
}
