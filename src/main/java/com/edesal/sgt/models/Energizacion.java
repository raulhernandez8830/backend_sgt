package com.edesal.sgt.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

/**
 * Created by DHernandez on 14/10/2019.
 */
@Entity
@Table(name = "SGT_energizacion")
public class Energizacion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;
    private String fecha_creacion;


    @OneToOne(mappedBy = "energizacion", cascade = CascadeType.ALL)
    @JsonIgnore
    DetalleManiobra detalleManiobra;




    @OneToMany(mappedBy = "energizacion", cascade = CascadeType.ALL)
    private List<DetallesEnergizacion> energizaciondt;


    public Energizacion() {
    }

    public Energizacion(String fecha_creacion, DetalleManiobra detalleManiobra, List<DetallesEnergizacion> energizaciondt) {
        this.fecha_creacion = fecha_creacion;
        this.detalleManiobra = detalleManiobra;
        this.energizaciondt = energizaciondt;
    }

    public DetalleManiobra getDetalleManiobra() {
        return detalleManiobra;
    }

    public void setDetalleManiobra(DetalleManiobra detalleManiobra) {
        this.detalleManiobra = detalleManiobra;
    }

    public List<DetallesEnergizacion> getEnergizaciondt() {
        return energizaciondt;
    }

    public void setEnergizaciondt(List<DetallesEnergizacion> energizaciondt) {
        this.energizaciondt = energizaciondt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }


}
