package com.edesal.sgt.models;

import javax.persistence.*;

/**
 * Created by DHernandez on 13/3/2020.
 */
@Entity
@Table(name="SGT_repusuariosSIGET")
public class RepUsuarioSIGET {

    @Id
    private Long id;
    private String idinter;
    private String idrepos;
    private String idusuario;
    private String tarifa;




    public String getIdinter() {
        return idinter;
    }

    public void setIdinter(String idinter) {
        this.idinter = idinter;
    }

    public String getIdrepos() {
        return idrepos;
    }

    public void setIdrepos(String idrepos) {
        this.idrepos = idrepos;
    }

    public String getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(String idusuario) {
        this.idusuario = idusuario;
    }

    public String getTarifa() {
        return tarifa;
    }

    public void setTarifa(String tarifa) {
        this.tarifa = tarifa;
    }

}
