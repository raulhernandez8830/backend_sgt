package com.edesal.sgt.models.SIGET;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by DHernandez on 12/5/2020.
 */
@Entity
@Table(name="SGT_compensacionesSIGET")
public class CompensacionSIGET {
    @Id
    private String id;
    private String idusuario;
    private String codoperacioncredito;
    private Double compensacionacreditada;



    public String getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(String idusuario) {
        this.idusuario = idusuario;
    }

    public String getCodoperacioncredito() {
        return codoperacioncredito;
    }

    public void setCodoperacioncredito(String codoperacioncredito) {
        this.codoperacioncredito = codoperacioncredito;
    }

    public Double getCompensacionacreditada() {
        return compensacionacreditada;
    }

    public void setCompensacionacreditada(Double compensacionacreditada) {
        this.compensacionacreditada = compensacionacreditada;
    }

}
