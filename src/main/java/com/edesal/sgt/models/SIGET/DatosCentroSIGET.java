package com.edesal.sgt.models.SIGET;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by DHernandez on 19/3/2020.
 */
@Entity
@Table(name = "SGT_datoscentrosSIGET")
public class DatosCentroSIGET {
    private String cenmtbt;
    private Long tensionservicio;
    private String tipoarrollamiento;
    private String tiposervicio;
    private String tipocon;
    private String numtrafo;
    private Long kvainst;
    private String direccion;
    private String municipio;
    private String departamento;
    private String sucursal;
    private String ssee;
    private String circuito;
    @Id
    private Long id;
    private Double coordenadax;
    private Double coordenaday;

    public String getCenmtbt() {
        return cenmtbt;
    }

    public void setCenmtbt(String cenmtbt) {
        this.cenmtbt = cenmtbt;
    }

    public Long getTensionservicio() {
        return tensionservicio;
    }

    public void setTensionservicio(Long tensionservicio) {
        this.tensionservicio = tensionservicio;
    }

    public String getTipoarrollamiento() {
        return tipoarrollamiento;
    }

    public void setTipoarrollamiento(String tipoarrollamiento) {
        this.tipoarrollamiento = tipoarrollamiento;
    }

    public String getTiposervicio() {
        return tiposervicio;
    }

    public void setTiposervicio(String tiposervicio) {
        this.tiposervicio = tiposervicio;
    }

    public String getTipocon() {
        return tipocon;
    }

    public void setTipocon(String tipocon) {
        this.tipocon = tipocon;
    }

    public String getNumtrafo() {
        return numtrafo;
    }

    public void setNumtrafo(String numtrafo) {
        this.numtrafo = numtrafo;
    }

    public Long getKvainst() {
        return kvainst;
    }

    public void setKvainst(Long kvainst) {
        this.kvainst = kvainst;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public String getSsee() {
        return ssee;
    }

    public void setSsee(String ssee) {
        this.ssee = ssee;
    }

    public String getCircuito() {
        return circuito;
    }

    public void setCircuito(String circuito) {
        this.circuito = circuito;
    }


    public Double getCoordenadax() {
        return coordenadax;
    }

    public void setCoordenadax(Double coordenadax) {
        this.coordenadax = coordenadax;
    }

    public Double getCoordenaday() {
        return coordenaday;
    }

    public void setCoordenaday(Double coordenaday) {
        this.coordenaday = coordenaday;
    }
}
