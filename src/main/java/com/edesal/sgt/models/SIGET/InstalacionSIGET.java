package com.edesal.sgt.models.SIGET;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by DHernandez on 6/5/2020.
 */
@Entity
@Table(name="SGT_instalacionesSIGET")
public class InstalacionSIGET {
    @Id
    private Long id;
    private String circuito;
    private String ssee;
    private Long trafosurb;
    private Long trafosrur;
    private Long kvainsurb;
    private Long kvainsrur;
    private Long potconturb;
    private Long potcontrur;




    public String getCircuito() {
        return circuito;
    }

    public void setCircuito(String circuito) {
        this.circuito = circuito;
    }

    public String getSsee() {
        return ssee;
    }

    public void setSsee(String ssee) {
        this.ssee = ssee;
    }

    public Long getTrafosurb() {
        return trafosurb;
    }

    public void setTrafosurb(Long trafosurb) {
        this.trafosurb = trafosurb;
    }

    public Long getTrafosrur() {
        return trafosrur;
    }

    public void setTrafosrur(Long trafosrur) {
        this.trafosrur = trafosrur;
    }

    public Long getKvainsurb() {
        return kvainsurb;
    }

    public void setKvainsurb(Long kvainsurb) {
        this.kvainsurb = kvainsurb;
    }

    public Long getKvainsrur() {
        return kvainsrur;
    }

    public void setKvainsrur(Long kvainsrur) {
        this.kvainsrur = kvainsrur;
    }

    public Long getPotconturb() {
        return potconturb;
    }

    public void setPotconturb(Long potconturb) {
        this.potconturb = potconturb;
    }

    public Long getPotcontrur() {
        return potcontrur;
    }

    public void setPotcontrur(Long potcontrur) {
        this.potcontrur = potcontrur;
    }
}
