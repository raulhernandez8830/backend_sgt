package com.edesal.sgt.models.SIGET;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by DHernandez on 16/3/2020.
 */
@Entity
@Table(name = "SGT_reclamosinterrSIGET")
public class ReclamoInterSIGET {

    @Id
    private String id;
    private String idusuario;
    private String codreclamo;
    private String fechare;
    private String idinter;
    private String codfalla;



    public String getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(String idusuario) {
        this.idusuario = idusuario;
    }

    public String getCodreclamo() {
        return codreclamo;
    }

    public void setCodreclamo(String codreclamo) {
        this.codreclamo = codreclamo;
    }

    public String getFechare() {
        return fechare;
    }

    public void setFechare(String fechare) {
        this.fechare = fechare;
    }

    public String getIdinter() {
        return idinter;
    }

    public void setIdinter(String idinter) {
        this.idinter = idinter;
    }

    public String getCodfalla() {
        return codfalla;
    }

    public void setCodfalla(String codfalla) {
        this.codfalla = codfalla;
    }
}
