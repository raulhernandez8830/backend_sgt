package com.edesal.sgt.models;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by DHernandez on 4/9/2019.
 */
@Entity
@Table(name = "ENS_CORTES")

public class Corte {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long sec_corte;

    private String codigo_distribuidora;
    private String codigo_red;
    private String codigo_corte;
    private String tipo_elemento;
    private String sec_circuito;
    private String descripcion;
    private Long nivel;
    private Long activo;
    private String ruta_imagen;
    private java.sql.Timestamp fecha_instalacion;
    private String tipo_fusible;
    private String calibre_fusible;
    private String direccion;
    private String fase;
    private String kva;
    private String id_vanmt;
    private String id_ssee;
    private String co_tieq;
    private String id_pos;
    private String cod_material;
    private String en_equmt;
    private String propiedad;
    private String cod_voltaje;
    private Double capacidad;
    private Long fase_vnr;
    private Long seccionalizador;
    private Long alimentador_principal;
    private Long alimentador_colonia;
    private String corte_padre;
    private Long zona_id;
    private Boolean corte_mt_ap;


    @OneToMany( cascade = CascadeType.ALL, mappedBy="corte")
    @JsonManagedReference
    private List<Transformador> transformadores;


    public List<Transformador> getTransformadores() {
        return transformadores;
    }

    public void setTransformadores(List<Transformador> transformadores) {
        this.transformadores = transformadores;
    }

    public String getCodigo_distribuidora() {
        return codigo_distribuidora;
    }

    public void setCodigo_distribuidora(String codigo_distribuidora) {
        this.codigo_distribuidora = codigo_distribuidora;
    }


    public Boolean getCorte_mt_ap() {
        return corte_mt_ap;
    }

    public void setCorte_mt_ap(Boolean corte_mt_ap) {
        this.corte_mt_ap = corte_mt_ap;
    }

    public String getCodigo_red() {
        return codigo_red;
    }

    public void setCodigo_red(String codigo_red) {
        this.codigo_red = codigo_red;
    }

    public String getCodigo_corte() {
        return codigo_corte;
    }

    public void setCodigo_corte(String codigo_corte) {
        this.codigo_corte = codigo_corte;
    }

    public String getTipo_elemento() {
        return tipo_elemento;
    }

    public void setTipo_elemento(String tipo_elemento) {
        this.tipo_elemento = tipo_elemento;
    }

    public Long getSec_corte() {
        return sec_corte;
    }

    public void setSec_corte(Long sec_corte) {
        this.sec_corte = sec_corte;
    }

    public String getSec_circuito() {
        return sec_circuito;
    }

    public void setSec_circuito(String sec_circuito) {
        this.sec_circuito = sec_circuito;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Long getNivel() {
        return nivel;
    }

    public void setNivel(Long nivel) {
        this.nivel = nivel;
    }

    public Long getActivo() {
        return activo;
    }

    public void setActivo(Long activo) {
        this.activo = activo;
    }

    public String getRuta_imagen() {
        return ruta_imagen;
    }

    public void setRuta_imagen(String ruta_imagen) {
        this.ruta_imagen = ruta_imagen;
    }

    public java.sql.Timestamp getFecha_instalacion() {
        return fecha_instalacion;
    }

    public void setFecha_instalacion(java.sql.Timestamp fecha_instalacion) {
        this.fecha_instalacion = fecha_instalacion;
    }

    public String getTipo_fusible() {
        return tipo_fusible;
    }

    public void setTipo_fusible(String tipo_fusible) {
        this.tipo_fusible = tipo_fusible;
    }

    public String getCalibre_fusible() {
        return calibre_fusible;
    }

    public void setCalibre_fusible(String calibre_fusible) {
        this.calibre_fusible = calibre_fusible;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getFase() {
        return fase;
    }

    public void setFase(String fase) {
        this.fase = fase;
    }

    public String getKva() {
        return kva;
    }

    public void setKva(String kva) {
        this.kva = kva;
    }

    public String getId_vanmt() {
        return id_vanmt;
    }

    public void setId_vanmt(String id_vanmt) {
        this.id_vanmt = id_vanmt;
    }

    public String getId_ssee() {
        return id_ssee;
    }

    public void setId_ssee(String id_ssee) {
        this.id_ssee = id_ssee;
    }

    public String getCo_tieq() {
        return co_tieq;
    }

    public void setCo_tieq(String co_tieq) {
        this.co_tieq = co_tieq;
    }

    public String getId_pos() {
        return id_pos;
    }

    public void setId_pos(String id_pos) {
        this.id_pos = id_pos;
    }

    public String getCod_material() {
        return cod_material;
    }

    public void setCod_material(String cod_material) {
        this.cod_material = cod_material;
    }

    public String getEn_equmt() {
        return en_equmt;
    }

    public void setEn_equmt(String en_equmt) {
        this.en_equmt = en_equmt;
    }

    public String getPropiedad() {
        return propiedad;
    }

    public void setPropiedad(String propiedad) {
        this.propiedad = propiedad;
    }

    public String getCod_voltaje() {
        return cod_voltaje;
    }

    public void setCod_voltaje(String cod_voltaje) {
        this.cod_voltaje = cod_voltaje;
    }

    public Double getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(Double capacidad) {
        this.capacidad = capacidad;
    }

    public Long getFase_vnr() {
        return fase_vnr;
    }

    public void setFase_vnr(Long fase_vnr) {
        this.fase_vnr = fase_vnr;
    }

    public Long getSeccionalizador() {
        return seccionalizador;
    }

    public void setSeccionalizador(Long seccionalizador) {
        this.seccionalizador = seccionalizador;
    }

    public Long getAlimentador_principal() {
        return alimentador_principal;
    }

    public void setAlimentador_principal(Long alimentador_principal) {
        this.alimentador_principal = alimentador_principal;
    }

    public Long getAlimentador_colonia() {
        return alimentador_colonia;
    }

    public void setAlimentador_colonia(Long alimentador_colonia) {
        this.alimentador_colonia = alimentador_colonia;
    }

    public String getCorte_padre() {
        return corte_padre;
    }

    public void setCorte_padre(String corte_padre) {
        this.corte_padre = corte_padre;
    }

    public Long getZona_id() {
        return zona_id;
    }

    public void setZona_id(Long zona_id) {
        this.zona_id = zona_id;
    }
}
