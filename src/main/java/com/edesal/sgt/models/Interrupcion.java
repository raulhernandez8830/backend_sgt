package com.edesal.sgt.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by DHernandez on 18/9/2019.
 */
@Entity

@Table(name = "ENS_INTERRUPCION")
public class Interrupcion {

    @Id
    @Column(name = "ID_INTERRUPCION")
    public Long id;

    private String periodo;
    private Long correlativo;
    private String tipo_interrupcion;
    private String origen_interrupcion;
    @ManyToOne
    @JoinColumn(name = "causa_interrupcion")
    Causa causa_interrupcion;
    private String tipo_sistema;
    private String codigo_dred;
    private String codigo_interrupcion;
    private String motivo_reportado;
    private String motivo_real;
    private String estado;
    private String fecha_ingreso;
    private String fecha_inicio;
    private String fecha_fin;
    private String fecha_cierre;

    private String elemento;
    private String acometidad;
    private Long   num_suministro;
    private String nombre_elemento;
    private String personal_atendio;
    private String fase;
    private String kwh_ut;
    private String status;
    private String calificacion;
    private String codigo_reposicion;
    private String tipo_tension;


    @OneToMany(mappedBy = "interrupcion", cascade = CascadeType.ALL)
    private List<DetalleInterrupcion> detallemaniobra;



    @OneToMany(mappedBy = "interrupcion", cascade = CascadeType.ALL)
    private List<Reclamo> reclamos;


    public List<Reclamo> getReclamos() {
        return reclamos;
    }

    public void setReclamos(List<Reclamo> reclamos) {
        this.reclamos = reclamos;
    }

    public Interrupcion() {
    }

    public Interrupcion(String tipo_tension,Long id,Integer id_maniobra, String periodo, Long correlativo, String tipo_interrupcion, String origen_interrupcion, Causa causa_interrupcion, String tipo_sistema, String codigo_dred, String codigo_interrupcion, String motivo_reportado, String motivo_real, String estado, String fecha_ingreso, String fecha_inicio, String fecha_fin, String fecha_cierre, String elemento, String acometidad, Long num_suministro, String nombre_elemento, String personal_atendio, String fase, String kwh_ut, String status, String calificacion) {
        this.periodo = periodo;
        this.tipo_tension = tipo_tension;
        this.correlativo = correlativo;
        this.tipo_interrupcion = tipo_interrupcion;
        this.origen_interrupcion = origen_interrupcion;
        this.causa_interrupcion = causa_interrupcion;
        this.tipo_sistema = tipo_sistema;
        this.codigo_dred = codigo_dred;
        this.codigo_interrupcion = codigo_interrupcion;
        this.motivo_reportado = motivo_reportado;
        this.motivo_real = motivo_real;
        this.estado = estado;
        this.fecha_ingreso = fecha_ingreso;
        this.fecha_inicio = fecha_inicio;
        this.fecha_fin = fecha_fin;
        this.fecha_cierre = fecha_cierre;

        this.elemento = elemento;
        this.acometidad = acometidad;
        this.num_suministro = num_suministro;
        this.nombre_elemento = nombre_elemento;
        this.personal_atendio = personal_atendio;
        this.fase = fase;
        this.kwh_ut = kwh_ut;
        this.status = status;
        this.calificacion = calificacion;


        this.id = id;


    }

    public String getTipo_tension() {
        return tipo_tension;
    }

    public void setTipo_tension(String tipo_tension) {
        this.tipo_tension = tipo_tension;
    }

    public String getCodigo_reposicion() {
        return codigo_reposicion;
    }

    public void setCodigo_reposicion(String codigo_reposicion) {
        this.codigo_reposicion = codigo_reposicion;
    }



    public List<DetalleInterrupcion> getDetallemaniobra() {
        return detallemaniobra;
    }

    public void setDetallemaniobra(List<DetalleInterrupcion> detallemaniobra) {
        this.detallemaniobra = detallemaniobra;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public Long getCorrelativo() {
        return correlativo;
    }

    public void setCorrelativo(Long correlativo) {
        this.correlativo = correlativo;
    }

    public String getTipo_interrupcion() {
        return tipo_interrupcion;
    }

    public void setTipo_interrupcion(String tipo_interrupcion) {
        this.tipo_interrupcion = tipo_interrupcion;
    }

    public String getOrigen_interrupcion() {
        return origen_interrupcion;
    }

    public void setOrigen_interrupcion(String origen_interrupcion) {
        this.origen_interrupcion = origen_interrupcion;
    }

    public Causa getCausa_interrupcion() {
        return causa_interrupcion;
    }

    public void setCausa_interrupcion(Causa causa_interrupcion) {
        this.causa_interrupcion = causa_interrupcion;
    }

    public String getTipo_sistema() {
        return tipo_sistema;
    }

    public void setTipo_sistema(String tipo_sistema) {
        this.tipo_sistema = tipo_sistema;
    }

    public String getCodigo_dred() {
        return codigo_dred;
    }

    public void setCodigo_dred(String codigo_dred) {
        this.codigo_dred = codigo_dred;
    }

    public String getCodigo_interrupcion() {
        return codigo_interrupcion;
    }

    public void setCodigo_interrupcion(String codigo_interrupcion) {
        this.codigo_interrupcion = codigo_interrupcion;
    }

    public String getMotivo_reportado() {
        return motivo_reportado;
    }

    public void setMotivo_reportado(String motivo_reportado) {
        this.motivo_reportado = motivo_reportado;
    }

    public String getMotivo_real() {
        return motivo_real;
    }

    public void setMotivo_real(String motivo_real) {
        this.motivo_real = motivo_real;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getFecha_ingreso() {
        return fecha_ingreso;
    }

    public void setFecha_ingreso(String fecha_ingreso) {
        this.fecha_ingreso = fecha_ingreso;
    }

    public String getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(String fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public String getFecha_fin() {
        return fecha_fin;
    }

    public void setFecha_fin(String fecha_fin) {
        this.fecha_fin = fecha_fin;
    }

    public String getFecha_cierre() {
        return fecha_cierre;
    }

    public void setFecha_cierre(String fecha_cierre) {
        this.fecha_cierre = fecha_cierre;
    }



    public String getElemento() {
        return elemento;
    }

    public void setElemento(String elemento) {
        this.elemento = elemento;
    }

    public String getAcometidad() {
        return acometidad;
    }

    public void setAcometidad(String acometidad) {
        this.acometidad = acometidad;
    }

    public Long getNum_suministro() {
        return num_suministro;
    }

    public void setNum_suministro(Long num_suministro) {
        this.num_suministro = num_suministro;
    }

    public String getNombre_elemento() {
        return nombre_elemento;
    }

    public void setNombre_elemento(String nombre_elemento) {
        this.nombre_elemento = nombre_elemento;
    }

    public String getPersonal_atendio() {
        return personal_atendio;
    }

    public void setPersonal_atendio(String personal_atendio) {
        this.personal_atendio = personal_atendio;
    }

    public String getFase() {
        return fase;
    }

    public void setFase(String fase) {
        this.fase = fase;
    }

    public String getKwh_ut() {
        return kwh_ut;
    }

    public void setKwh_ut(String kwh_ut) {
        this.kwh_ut = kwh_ut;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(String calificacion) {
        this.calificacion = calificacion;
    }



}
