package com.edesal.sgt.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by DHernandez on 28/8/2020.
 */
@Entity
@Table(name="FE_SECUENCIAL")
public class Secuencial {
    @Id
    private Long codigo_secuencia;
    private String nombre;
    private Long secuencial;

    public Long getCodigo_secuencia() {
        return codigo_secuencia;
    }

    public void setCodigo_secuencia(Long codigo_secuencia) {
        this.codigo_secuencia = codigo_secuencia;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Long getSecuencial() {
        return secuencial;
    }

    public void setSecuencial(Long secuencial) {
        this.secuencial = secuencial;
    }
}
