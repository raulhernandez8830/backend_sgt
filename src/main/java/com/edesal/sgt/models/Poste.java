package com.edesal.sgt.models;

/**
 * Created by DHernandez on 18/5/2020.
 */
public class Poste {
    private String id_pos;
    private Double coord_x;
    private Double coord_y;
    private String ubicacion;
    private String area_tendido;

    public Poste() {
    }

    public String getId_pos() {
        return id_pos;
    }

    public void setId_pos(String id_pos) {
        this.id_pos = id_pos;
    }

    public Double getCoord_x() {
        return coord_x;
    }

    public void setCoord_x(Double coord_x) {
        this.coord_x = coord_x;
    }

    public Double getCoord_y() {
        return coord_y;
    }

    public void setCoord_y(Double coord_y) {
        this.coord_y = coord_y;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getArea_tendido() {
        return area_tendido;
    }

    public void setArea_tendido(String area_tendido) {
        this.area_tendido = area_tendido;
    }
}
