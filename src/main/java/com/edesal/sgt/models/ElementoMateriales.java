package com.edesal.sgt.models;

/**
 * Created by DHernandez on 24/5/2020.
 */
public class ElementoMateriales {
    String elemento;

    public String getElemento() {
        return elemento;
    }

    public void setElemento(String elemento) {
        this.elemento = elemento;
    }
}
