package com.edesal.sgt.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

/**
 * Created by DHernandez on 12/11/2019.
 */
@Entity
@Table(name = "SGT_detalle_interrupcion")
public class DetalleInterrupcion {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    Long itinerario;

    public Long getItinerario() {
        return itinerario;
    }

    public void setItinerario(Long itinerario) {
        this.itinerario = itinerario;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "interrupcion")
    @JsonIgnore
    private Interrupcion interrupcion;

    private String codigo_reposicion;




    public DetalleInterrupcion() {
    }

    public String getCodigo_reposicion() {
        return codigo_reposicion;
    }

    public void setCodigo_reposicion(String codigo_reposicion) {
        this.codigo_reposicion = codigo_reposicion;
    }

    public Interrupcion getInterrupcion() {
        return interrupcion;
    }

    public void setInterrupcion_(Interrupcion interrupcion) {
        this.interrupcion = interrupcion;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


}
