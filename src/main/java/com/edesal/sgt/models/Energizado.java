package com.edesal.sgt.models;

import javax.persistence.*;
import java.util.List;

/**
 * Created by DHernandez on 7/10/2019.
 */
@Table(name="SGT_energizados")
@Entity
public class Energizado {

    @Id
    private Long id;
    private String corte_aperturado;
    private String corte_cerrado;
    private String descripcion;


    @OneToMany(mappedBy = "energizado", cascade = CascadeType.ALL)
    private List<DetalleEnergizado> configuracion;

    public Energizado() {
    }

    public List<DetalleEnergizado> getDetalleenergizado() {
        return configuracion;
    }

    public void setDetalleenergizado(List<DetalleEnergizado> detalleenergizado) {
        this.configuracion = detalleenergizado;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCorte_aperturado() {
        return corte_aperturado;
    }

    public void setCorte_aperturado(String corte_aperturado) {
        this.corte_aperturado = corte_aperturado;
    }

    public String getCorte_cerrado() {
        return corte_cerrado;
    }

    public void setCorte_cerrado(String corte_cerrado) {
        this.corte_cerrado = corte_cerrado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
