package com.edesal.sgt.models;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.math.BigDecimal;

/**
 * Created by DHernandez on 4/3/2020.
 */
@Entity
public class InterrupcionSuministro {


    @EmbeddedId
    private Integer id;

    private String interrupcion;
    private Integer conteo;

    public String getInterrupcion() {
        return interrupcion;
    }

    public void setInterrupcion(String interrupcion) {
        this.interrupcion = interrupcion;
    }

    public Integer getConteo() {
        return conteo;
    }

    public void setConteo(Integer conteo) {
        this.conteo = conteo;
    }
}
