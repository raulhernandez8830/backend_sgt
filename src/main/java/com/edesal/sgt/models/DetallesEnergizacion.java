package com.edesal.sgt.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

/**
 * Created by DHernandez on 14/10/2019.
 */
@Entity
@Table(name="SGT_dt_energizacion")
public class DetallesEnergizacion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String corte;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    @JoinColumn(name = "energizacion")
    private Energizacion energizacion;

    public DetallesEnergizacion() {
    }

    public Energizacion getEnergizacion() {
        return energizacion;
    }

    public void setEnergizacion(Energizacion energizacion) {
        this.energizacion = energizacion;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



    public String getCorte() {
        return corte;
    }

    public void setCorte(String corte) {
        this.corte = corte;
    }
}
