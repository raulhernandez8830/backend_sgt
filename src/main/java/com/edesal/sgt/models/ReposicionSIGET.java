package com.edesal.sgt.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by DHernandez on 6/3/2020.
 */
@Entity
@Table(name = "SGT_reposicionesSIGET")
public class ReposicionSIGET {
    @Id
    private Long id;
    private String idinter;
    private String idrepos;
    private String fecharp;
    private String idelem;
    private String tipoelem;
    private String ssee;
    private String circuito;



    public String getIdinter() {
        return idinter;
    }

    public void setIdinter(String idinter) {
        this.idinter = idinter;
    }

    public String getIdrepos() {
        return idrepos;
    }

    public void setIdrepos(String idrepos) {
        this.idrepos = idrepos;
    }

    public String getFecharp() {
        return fecharp;
    }

    public void setFecharp(String fecharp) {
        this.fecharp = fecharp;
    }

    public String getIdelem() {
        return idelem;
    }

    public void setIdelem(String idelem) {
        this.idelem = idelem;
    }

    public String getTipoelem() {
        return tipoelem;
    }

    public void setTipoelem(String tipoelem) {
        this.tipoelem = tipoelem;
    }

    public String getSsee() {
        return ssee;
    }

    public void setSsee(String ssee) {
        this.ssee = ssee;
    }

    public String getCircuito() {
        return circuito;
    }

    public void setCircuito(String circuito) {
        this.circuito = circuito;
    }
}
