package com.edesal.sgt.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by DHernandez on 20/12/2019.
 */
@Entity
@Table(name = "temp_ens")
public class UsuarioAfectado {
    private String num_suministro;
    private String interrupcion;
    private String trafo;
    private Long duracion;
    @Id
    private String id;
    private java.sql.Timestamp fecha_inst_trafo;
    private java.sql.Timestamp fecha_inst_nis;
    private java.sql.Timestamp fecha_maniobra;
    private String origen;
    private java.sql.Timestamp fecha_fin_interrup;
    private String codigo_interrupcion;

    public String getNum_suministro() {
        return num_suministro;
    }

    public void setNum_suministro(String num_suministro) {
        this.num_suministro = num_suministro;
    }

    public String getInterrupcion() {
        return interrupcion;
    }

    public void setInterrupcion(String interrupcion) {
        this.interrupcion = interrupcion;
    }

    public String getTrafo() {
        return trafo;
    }

    public void setTrafo(String trafo) {
        this.trafo = trafo;
    }

    public Long getDuracion() {
        return duracion;
    }

    public void setDuracion(Long duracion) {
        this.duracion = duracion;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public java.sql.Timestamp getFecha_inst_trafo() {
        return fecha_inst_trafo;
    }

    public void setFecha_inst_trafo(java.sql.Timestamp fecha_inst_trafo) {
        this.fecha_inst_trafo = fecha_inst_trafo;
    }

    public java.sql.Timestamp getFecha_inst_nis() {
        return fecha_inst_nis;
    }

    public void setFecha_inst_nis(java.sql.Timestamp fecha_inst_nis) {
        this.fecha_inst_nis = fecha_inst_nis;
    }

    public java.sql.Timestamp getFecha_maniobra() {
        return fecha_maniobra;
    }

    public void setFecha_maniobra(java.sql.Timestamp fecha_maniobra) {
        this.fecha_maniobra = fecha_maniobra;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public java.sql.Timestamp getFecha_fin_interrup() {
        return fecha_fin_interrup;
    }

    public void setFecha_fin_interrup(java.sql.Timestamp fecha_fin_interrup) {
        this.fecha_fin_interrup = fecha_fin_interrup;
    }

    public String getCodigo_interrupcion() {
        return codigo_interrupcion;
    }

    public void setCodigo_interrupcion(String codigo_interrupcion) {
        this.codigo_interrupcion = codigo_interrupcion;
    }
}
