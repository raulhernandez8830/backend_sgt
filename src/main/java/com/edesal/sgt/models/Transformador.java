package com.edesal.sgt.models;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import java.util.List;

/**
 * Created by DHernandez on 28/2/2020.
 */
@Entity
@Table(name="ENS_TRANSFORMADORES")

public class Transformador {
    @Id
    private Long sec_trafo;

    private String codigo_distribuidora;
    private String codigo_red;
    private String codigo_trafo;
    private String tipo_elemento;
    private String cod_voltaje_s;
    private String cod_voltaje_p;
    private String sec_circuito;
    private String descripcion_trafo;
    private String bandera_activo;
    private String kva;
    private String fase;
    private Long exclusivo;
    private String fecha_instalacion;
    private String marca;
    private String calibre_fusible;
    private Long perdida_vacio;
    private Long perdida_carga;
    private String tipo_servicio;
    private String id_vanmt;
    private String id_pos;
    private String propiedad;
    private String area_tendido;
    private String nfases_p;
    private String fases_p;
    private String cod_material;

    private String conductores;

    private String pos_x;
    private String pos_y;
    private String cant_trafos;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sec_corte")
    @JsonBackReference
    private Corte corte;

    private String usuario_creacion;
    private String usuario_edicion;
    private String usuario_baja;
    private String comentario_baja;
    private String fecha_baja;
    private String fecha_baja_proceso;


    @OneToMany(mappedBy = "transformador", cascade = CascadeType.ALL)
    private List<DetalleCT> detalles;



    public Transformador() {
    }

    public List<DetalleCT> getDetalles() {
        return detalles;
    }

    public void setDetalles(List<DetalleCT> detalles) {
        this.detalles = detalles;
    }

    public String getUsuario_creacion() {
        return usuario_creacion;
    }

    public String getFecha_baja_proceso() {
        return fecha_baja_proceso;
    }

    public void setFecha_baja_proceso(String fecha_baja_proceso) {
        this.fecha_baja_proceso = fecha_baja_proceso;
    }

    public void setUsuario_creacion(String usuario_creacion) {
        this.usuario_creacion = usuario_creacion;
    }

    public String getUsuario_edicion() {
        return usuario_edicion;
    }

    public void setUsuario_edicion(String usuario_edicion) {
        this.usuario_edicion = usuario_edicion;
    }

    public String getUsuario_baja() {
        return usuario_baja;
    }

    public void setUsuario_baja(String usuario_baja) {
        this.usuario_baja = usuario_baja;
    }

    public String getComentario_baja() {
        return comentario_baja;
    }

    public void setComentario_baja(String comentario_baja) {
        this.comentario_baja = comentario_baja;
    }

    public String getFecha_baja() {
        return fecha_baja;
    }

    public void setFecha_baja(String fecha_baja) {
        this.fecha_baja = fecha_baja;
    }

    public String getCod_voltaje_s() {
        return cod_voltaje_s;
    }

    public void setCod_voltaje_s(String cod_voltaje_s) {
        this.cod_voltaje_s = cod_voltaje_s;
    }

    public String getCod_voltaje_p() {
        return cod_voltaje_p;
    }

    public void setCod_voltaje_p(String cod_voltaje_p) {
        this.cod_voltaje_p = cod_voltaje_p;
    }

    public String getCodigo_distribuidora() {
        return codigo_distribuidora;
    }

    public void setCodigo_distribuidora(String codigo_distribuidora) {
        this.codigo_distribuidora = codigo_distribuidora;
    }

    public Corte getCorte() {
        return corte;
    }

    public void setCorte(Corte corte) {
        this.corte = corte;
    }

    public String getCodigo_red() {
        return codigo_red;
    }

    public void setCodigo_red(String codigo_red) {
        this.codigo_red = codigo_red;
    }

    public String getCodigo_trafo() {
        return codigo_trafo;
    }

    public void setCodigo_trafo(String codigo_trafo) {
        this.codigo_trafo = codigo_trafo;
    }

    public String getTipo_elemento() {
        return tipo_elemento;
    }

    public void setTipo_elemento(String tipo_elemento) {
        this.tipo_elemento = tipo_elemento;
    }

    public Long getSec_trafo() {
        return sec_trafo;
    }

    public void setSec_trafo(Long sec_trafo) {
        this.sec_trafo = sec_trafo;
    }



    public String getSec_circuito() {
        return sec_circuito;
    }

    public void setSec_circuito(String sec_circuito) {
        this.sec_circuito = sec_circuito;
    }

    public String getDescripcion_trafo() {
        return descripcion_trafo;
    }

    public void setDescripcion_trafo(String descripcion_trafo) {
        this.descripcion_trafo = descripcion_trafo;
    }

    public String getBandera_activo() {
        return bandera_activo;
    }

    public void setBandera_activo(String bandera_activo) {
        this.bandera_activo = bandera_activo;
    }

    public String getKva() {
        return kva;
    }

    public void setKva(String kva) {
        this.kva = kva;
    }

    public String getFase() {
        return fase;
    }

    public void setFase(String fase) {
        this.fase = fase;
    }

    public Long getExclusivo() {
        return exclusivo;
    }

    public void setExclusivo(Long exclusivo) {
        this.exclusivo = exclusivo;
    }

    public String getFecha_instalacion() {
        return fecha_instalacion;
    }

    public void setFecha_instalacion(String fecha_instalacion) {
        this.fecha_instalacion = fecha_instalacion;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getCalibre_fusible() {
        return calibre_fusible;
    }

    public void setCalibre_fusible(String calibre_fusible) {
        this.calibre_fusible = calibre_fusible;
    }

    public Long getPerdida_vacio() {
        return perdida_vacio;
    }

    public void setPerdida_vacio(Long perdida_vacio) {
        this.perdida_vacio = perdida_vacio;
    }

    public Long getPerdida_carga() {
        return perdida_carga;
    }

    public void setPerdida_carga(Long perdida_carga) {
        this.perdida_carga = perdida_carga;
    }

    public String getTipo_servicio() {
        return tipo_servicio;
    }

    public void setTipo_servicio(String tipo_servicio) {
        this.tipo_servicio = tipo_servicio;
    }

    public String getId_vanmt() {
        return id_vanmt;
    }

    public void setId_vanmt(String id_vanmt) {
        this.id_vanmt = id_vanmt;
    }

    public String getId_pos() {
        return id_pos;
    }

    public void setId_pos(String id_pos) {
        this.id_pos = id_pos;
    }

    public String getPropiedad() {
        return propiedad;
    }

    public void setPropiedad(String propiedad) {
        this.propiedad = propiedad;
    }

    public String getArea_tendido() {
        return area_tendido;
    }

    public void setArea_tendido(String area_tendido) {
        this.area_tendido = area_tendido;
    }

    public String getNfases_p() {
        return nfases_p;
    }

    public void setNfases_p(String nfases_p) {
        this.nfases_p = nfases_p;
    }

    public String getFases_p() {
        return fases_p;
    }

    public void setFases_p(String fases_p) {
        this.fases_p = fases_p;
    }

    public String getCod_material() {
        return cod_material;
    }

    public void setCod_material(String cod_material) {
        this.cod_material = cod_material;
    }



    public String getConductores() {
        return conductores;
    }

    public void setConductores(String conductores) {
        this.conductores = conductores;
    }



    public String getPos_x() {
        return pos_x;
    }

    public void setPos_x(String pos_x) {
        this.pos_x = pos_x;
    }

    public String getPos_y() {
        return pos_y;
    }

    public void setPos_y(String pos_y) {
        this.pos_y = pos_y;
    }

    public String getCant_trafos() {
        return cant_trafos;
    }

    public void setCant_trafos(String cant_trafos) {
        this.cant_trafos = cant_trafos;
    }
}
