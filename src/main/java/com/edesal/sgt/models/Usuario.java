package com.edesal.sgt.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by DHernandez on 19/8/2019.
 */
@Entity
@Table(name="SGT_usuarios")
public class Usuario {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private String alias;
    private String password;
    private java.sql.Timestamp fecha_creacion;
    private String estado;
    private String nombre;
    private String apellido;




    @ManyToMany(cascade =
            {CascadeType.MERGE})
    @JoinTable(name = "SGT_usuario_rol",
            joinColumns = @JoinColumn(name = "usuario"),
            inverseJoinColumns = @JoinColumn(name = "rol")
    )
    private List<Rol> roles = new ArrayList<Rol>();

    public Long getId() {
        return id;
    }



    public void setId(Long id) {
        this.id = id;
    }

    public List<Rol> getRoles() {
        return roles;
    }

    public void setRoles(List<Rol> roles) {
        this.roles = roles;
    }

    //constructor
    public Usuario(List<Rol> roles, Long id, String alias, String password, Timestamp fecha_creacion, String estado, String nombre, String apellido) {
        this.id = id;
        this.alias = alias;
        this.password = password;
        this.fecha_creacion = fecha_creacion;
        this.estado = estado;
        this.nombre = nombre;
        this.apellido = apellido;
        this.roles = roles;


    }

    public Usuario() {

    }



    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public java.sql.Timestamp getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(java.sql.Timestamp fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }




}
