package com.edesal.sgt.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by DHernandez on 9/3/2020.
 */
@Entity
@Table(name = "SGT_centrosMTBT")
public class CentroMTBT {
    private String idinter;
    private String idrepos;
    private String cenmtbt;
    private Long kva;
    private String tiposervicio;
    @Id
    private Long id;

    public String getIdinter() {
        return idinter;
    }

    public void setIdinter(String idinter) {
        this.idinter = idinter;
    }

    public String getIdrepos() {
        return idrepos;
    }

    public void setIdrepos(String idrepos) {
        this.idrepos = idrepos;
    }

    public String getCenmtbt() {
        return cenmtbt;
    }

    public void setCenmtbt(String cenmtbt) {
        this.cenmtbt = cenmtbt;
    }

    public Long getKva() {
        return kva;
    }

    public void setKva(Long kva) {
        this.kva = kva;
    }

    public String getTiposervicio() {
        return tiposervicio;
    }

    public void setTiposervicio(String tiposervicio) {
        this.tiposervicio = tiposervicio;
    }


}
