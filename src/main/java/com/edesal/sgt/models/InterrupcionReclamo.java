package com.edesal.sgt.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by DHernandez on 23/1/2020.
 */
@Entity
@Table(name = "ENS_INTERRUPCION_SUMINISTRO")
public class InterrupcionReclamo {

    @Id
    private String secuencial;
    private String id_interupcion;
    private String num_suministro;
    private String correlativo;
    private String cadena_cod;
    private String cadena_nombre;
    private String kwh;
    private String ens;

    public String getSecuencial() {
        return secuencial;
    }

    public void setSecuencial(String secuencial) {
        this.secuencial = secuencial;
    }

    public String getId_interupcion() {
        return id_interupcion;
    }

    public void setId_interupcion(String id_interupcion) {
        this.id_interupcion = id_interupcion;
    }

    public String getNum_suministro() {
        return num_suministro;
    }

    public void setNum_suministro(String num_suministro) {
        this.num_suministro = num_suministro;
    }

    public String getCorrelativo() {
        return correlativo;
    }

    public void setCorrelativo(String correlativo) {
        this.correlativo = correlativo;
    }

    public String getCadena_cod() {
        return cadena_cod;
    }

    public void setCadena_cod(String cadena_cod) {
        this.cadena_cod = cadena_cod;
    }

    public String getCadena_nombre() {
        return cadena_nombre;
    }

    public void setCadena_nombre(String cadena_nombre) {
        this.cadena_nombre = cadena_nombre;
    }

    public String getKwh() {
        return kwh;
    }

    public void setKwh(String kwh) {
        this.kwh = kwh;
    }

    public String getEns() {
        return ens;
    }

    public void setEns(String ens) {
        this.ens = ens;
    }
}
