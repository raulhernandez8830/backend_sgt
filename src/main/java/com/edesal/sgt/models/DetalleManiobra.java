package com.edesal.sgt.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by DHernandez on 23/8/2019.
 */
@Entity
@Table(name="SGT_detalles_maniobra")
public class DetalleManiobra {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String hora_apertura;
    private Integer asociada;
    private String elemento_aperturado;
    private String fase_afectada;
    private String observaciones;
    private String corte_otro;
    private String transferencia;
    private String hora_cierre;
    private BigDecimal elemento_id;
    private Long transferencia_id;
    private String elemento_cierre;
    private String fase_afectada_cierre;
    private Integer afecta_usuarios;
    private Integer genera_reposicion;
    private BigDecimal trafo;
    private Integer duracion;


    public String getHora_cierre() {
        return hora_cierre;
    }

    public void setHora_cierre(String hora_cierre) {
        this.hora_cierre = hora_cierre;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "maniobra_id")
    @JsonIgnore
    private Maniobra maniobra;



    @OneToOne
    @JoinColumn(name = "energizacion", referencedColumnName = "id")
    private Energizacion energizacion;

    public Integer isAsociada() {
        return asociada;
    }

    public void setAsociada(Integer asociada) {
        this.asociada = asociada;
    }

    public Integer getAfecta_usuarios() {
        return afecta_usuarios;
    }

    public Integer getAsociada() {
        return asociada;
    }



    public Integer getDuracion() {
        return duracion;
    }

    public void setDuracion(Integer duracion) {
        this.duracion = duracion;
    }

    public Integer getGenera_reposicion() {
        return genera_reposicion;
    }


    public BigDecimal getTrafo() {
        return trafo;
    }

    public void setTrafo(BigDecimal trafo) {
        this.trafo = trafo;
    }

    public Integer isAfecta_usuarios() {
        return afecta_usuarios;
    }

    public void setAfecta_usuarios(Integer afecta_usuarios) {
        this.afecta_usuarios = afecta_usuarios;
    }

    public Integer isGenera_reposicion() {
        return genera_reposicion;
    }

    public void setGenera_reposicion(Integer genera_reposicion) {
        this.genera_reposicion = genera_reposicion;
    }


    public Energizacion getEnergizacion() {
        return energizacion;
    }

    public void setEnergizacion(Energizacion energizacion) {
        this.energizacion = energizacion;
    }

    public Maniobra getManiobra() {
        return maniobra;
    }

    public void setManiobra(Maniobra maniobra) {
        this.maniobra = maniobra;
    }

    public DetalleManiobra() {

    }




    public DetalleManiobra(Energizacion energizacion, String fase_afectada_cierre, String elemento_cierre, Long transferencia_id, Long id, String hora_apertura, String elemento_aperturado, String fase_afectada, String observaciones, String corte_otro, String transferencia, String hora_cierre, Maniobra maniobra) {
        this.id = id;
        this.hora_apertura = hora_apertura;
        this.elemento_aperturado = elemento_aperturado;
        this.fase_afectada = fase_afectada;
        this.observaciones = observaciones;
        this.corte_otro = corte_otro;
        this.transferencia = transferencia;
        this.hora_cierre = hora_cierre;
        this.maniobra = maniobra;
        this.transferencia_id = transferencia_id;
        this.elemento_cierre = elemento_cierre;
        this.fase_afectada_cierre = fase_afectada_cierre;
        this.energizacion = energizacion;

    }



    public String getFase_afectada_cierre() {
        return fase_afectada_cierre;
    }

    public void setFase_afectada_cierre(String fase_afectada_cierre) {
        this.fase_afectada_cierre = fase_afectada_cierre;
    }

    public String getElemento_cierre() {
        return elemento_cierre;
    }

    public void setElemento_cierre(String elemento_cierre) {
        this.elemento_cierre = elemento_cierre;
    }

    public BigDecimal getElemento_id() {
        return elemento_id;
    }

    public void setElemento_id(BigDecimal elemento_id) {
        this.elemento_id = elemento_id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Long getTransferencia_id() {
        return transferencia_id;
    }

    public void setTransferencia_id(Long transferencia_id) {
        this.transferencia_id = transferencia_id;
    }

    public String getHora_apertura() {
        return hora_apertura;
    }

    public void setHora_apertura(String hora_apertura) {
        this.hora_apertura = hora_apertura;
    }

    public String getElemento_aperturado() {
        return elemento_aperturado;
    }

    public void setElemento_aperturado(String elemento_aperturado) {
        this.elemento_aperturado = elemento_aperturado;
    }

    public String getFase_afectada() {
        return fase_afectada;
    }

    public void setFase_afectada(String fase_afectada) {
        this.fase_afectada = fase_afectada;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getCorte_otro() {
        return corte_otro;
    }

    public void setCorte_otro(String corte_otro) {
        this.corte_otro = corte_otro;
    }

    public String getTransferencia() {
        return transferencia;
    }

    public void setTransferencia(String transferencia) {
        this.transferencia = transferencia;
    }
}
