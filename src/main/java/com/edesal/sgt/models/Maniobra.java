package com.edesal.sgt.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by DHernandez on 23/8/2019.
 */
@Entity
@Table(name="SGT_maniobras")
public class Maniobra {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String red_modificada;
    private String fecha_ingreso; private String fecha_evento_inicio;
    private String fecha_evento_fin;

    private int estado;

    private String tipo_interrupcion;


    @OneToMany(mappedBy = "maniobra", cascade = CascadeType.ALL)
    private List<DetalleManiobra> detallesmaniobra;





    @ManyToOne
    @JoinColumn(name = "causa_interrupcion")
    Causa causa;

    //constructor
    public Maniobra() {

    }


    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "operador_responsable")
    Usuario usuario;




    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public Causa getCausa() {
        return causa;
    }

    public void setCausa(Causa causa) {
        this.causa = causa;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public List<DetalleManiobra> getDetallesmaniobra() {
        return detallesmaniobra;
    }

    public void setDetallesmaniobra(List<DetalleManiobra> detallesmaniobra) {
        this.detallesmaniobra = detallesmaniobra;
    }




    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRed_modificada() {
        return red_modificada;
    }

    public void setRed_modificada(String red_modificada) {
        this.red_modificada = red_modificada;
    }

    public String getFecha_ingreso() {
        return fecha_ingreso;
    }

    public void setFecha_ingreso(String fecha_ingreso) {
        this.fecha_ingreso = fecha_ingreso;
    }

    public String getFecha_evento_inicio() {
        return fecha_evento_inicio;
    }

    public void setFecha_evento_inicio(String fecha_evento_inicio) {
        this.fecha_evento_inicio = fecha_evento_inicio;
    }

    public String getFecha_evento_fin() {
        return fecha_evento_fin;
    }

    public void setFecha_evento_fin(String fecha_evento_fin) {
        this.fecha_evento_fin = fecha_evento_fin;
    }


    public String getTipo_interrupcion() {
        return tipo_interrupcion;
    }

    public void setTipo_interrupcion(String tipo_interrupcion) {
        this.tipo_interrupcion = tipo_interrupcion;
    }


}