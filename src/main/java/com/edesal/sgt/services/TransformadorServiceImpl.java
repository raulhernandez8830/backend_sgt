package com.edesal.sgt.services;

import com.edesal.sgt.dao.DetalleCTDAO;
import com.edesal.sgt.dao.TransformadorDAO;
import com.edesal.sgt.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by DHernandez on 15/5/2020.
 */
@Service
public class TransformadorServiceImpl implements TransformadorService {

    @PersistenceContext
    EntityManager em;

    @Autowired
    TransformadorDAO transformadordao;

    @Autowired
    DetalleCTDAO detallectdao;

    @Override
    public List<Transformador> getTrafos() {
        Query q = em.createQuery("SELECT t FROM Transformador t  ORDER BY t.sec_trafo DESC");
        List<Transformador> transformadores = q.getResultList();

        return transformadores;
    }

    @Override
    public List<Poste>  getPosteByTrafo(String poste) {

        List<Poste> posteslista = new ArrayList<>();

        Query q = em.createNativeQuery("SELECT p.id_pos, p.coord_X, p.coord_Y, p.ubicacion, p.area_tendido FROM FACTURACION.dbo.act_PostePozo p\n" +
                "  WHERE p.ID_Pos=:poste");
        q.setParameter("poste",poste);

        List<Object[]> postesobject = q.getResultList();


        for (Object[] postedto: postesobject) {
          Poste p = new Poste();
          String id_pos = (String) postedto[0];
          Double coord_x = Double.parseDouble(postedto[1].toString());
          Double coord_y = Double.parseDouble(postedto[2].toString());
          String ubicacion = (String) postedto[3];
          String area_tendido = (String) postedto[4];

          p.setId_pos(id_pos);
          p.setCoord_x(coord_x);
          p.setCoord_y(coord_y);
          p.setUbicacion(ubicacion);
          p.setArea_tendido(area_tendido);

          posteslista.add(p);

        }




        return posteslista;
    }

    @Override
    public Circuito getCircuitoByTrafo(String circuito) {
        Query q = em.createQuery("SELECT c FROM Circuito c WHERE c.sec_circuito=:c");
        q.setParameter("c",circuito);
        Circuito circuitodto = (Circuito) q.getSingleResult();

        return circuitodto;
    }


    @Override
    public List<Circuito> getAllCircuitos() {
        Query q = em.createQuery("SELECT c FROM Circuito c");
        List<Circuito> circuitos = q.getResultList();

        return circuitos;
    }


    @Override
    public Transformador saveTrafo(Transformador trafo) {

        // obtener el ultimo id ingresado
        Query q = em.createNativeQuery("SELECT TOP 1 t.SEC_TRAFO FROM ENS_TRANSFORMADORES t ORDER BY t.SEC_TRAFO DESC ");
        BigDecimal cod = (BigDecimal) q.getSingleResult();
        BigDecimal id = cod.add(new BigDecimal(1));


        Transformador trafodto = new Transformador();

        //establecemos la bandera del trafo en activo
        trafo.setBandera_activo("1");

        // establecemos codigo de distribuidora
        trafo.setCodigo_distribuidora("0006");

        trafo.setCodigo_red("1");
        trafo.setTipo_elemento("TR");

        trafo.setSec_trafo(id.longValue());



        // persistimos
        try {
            trafodto = transformadordao.save(trafo);
            if(trafodto.getDetalles().size() > 0) {
                for(Integer i=0; i<trafodto.getDetalles().size(); i++) {
                    DetalleCT detalle = new DetalleCT();
                    detalle = trafodto.getDetalles().get(i);
                    detalle.setTransformador(trafodto);

                    detallectdao.save(detalle);
                }
            }
        }catch(NoResultException e) {
            System.out.println("Error al guardar trafo" + e.getMessage());
        }

        return trafodto;
    }


    @Override
    public List<String> getPostes() {
        List<String> posteslista = new ArrayList<>();
        Query q = em.createNativeQuery("SELECT p.id_pos FROM FACTURACION.dbo.act_PostePozo p");
        posteslista = q.getResultList();



        return posteslista;

    }

    @Override
    public List<Voltaje> getVoltajes() {
        Query q = em.createQuery("SELECT v FROM Voltaje v");
        List<Voltaje> voltajes = q.getResultList();

        return voltajes;
    }

    @Override
    public List<MaterialesEstructura> getEsctructurasMateriales() {
        Query q = em.createNativeQuery("SELECT * FROM ens_materiales_estructura", MaterialesEstructura.class);
        List<MaterialesEstructura> estructuras = q.getResultList();

        return estructuras;
    }

    // actualizar informacion de transformador
    @Override
    public Transformador update(Transformador trafo) {
       Transformador transformadordto = new Transformador();


       try{
           Query q = em.createNativeQuery("SELECT *FROM ENS_TRANSFORMADORES  t WHERE t.CODIGO_TRAFO=:codigo",Transformador.class);
           q.setParameter("codigo", trafo.getCodigo_trafo());
           transformadordto = (Transformador) q.getSingleResult();
           trafo.setSec_trafo(transformadordto.getSec_trafo());
           trafo.setBandera_activo("1");

           // establecemos codigo de distribuidora
           trafo.setCodigo_distribuidora("0006");

           trafo.setCodigo_red("1");
           trafo.setTipo_elemento("TR");
           transformadordto = transformadordao.save(trafo);
       }catch(NoResultException E) {
           System.out.println("Error al actualizar trafo" + E.getMessage());
       }

       return transformadordto;

    }


    @Override
    public Transformador bajaTrafo(Transformador trafo) {
        Transformador transformadordto = new Transformador();


        try{
            Query q = em.createNativeQuery("SELECT *FROM ENS_TRANSFORMADORES  t WHERE t.CODIGO_TRAFO=:codigo",Transformador.class);
            q.setParameter("codigo", trafo.getCodigo_trafo());
            transformadordto = (Transformador) q.getSingleResult();
            trafo.setSec_trafo(transformadordto.getSec_trafo());
            trafo.setBandera_activo("0");

            // establecemos codigo de distribuidora
            trafo.setCodigo_distribuidora("0006");

            trafo.setCodigo_red("1");
            trafo.setTipo_elemento("TR");
            transformadordto = transformadordao.save(trafo);
        }catch(NoResultException E) {
            System.out.println("Error al actualizar trafo" + E.getMessage());
        }

        return transformadordto;
    }

    @Override
    public List<BigDecimal> suministrosActivosTrafo(String codigo) {
        Query q = em.createNativeQuery("SELECT fs.num_suministro FROM FACTURACION.dbo.FE_APARATOS a\n" +
                "INNER JOIN FACTURACION.dbo.FE_SUMINISTROS fs ON fs.num_suministro = a.num_suministro\n" +
                "WHERE a.CODIGO_ELEMENTO=:codigo AND a.BANDERA_ACTIVO = 1 AND fs.estado = 'A'");
        q.setParameter("codigo",codigo);

        List<BigDecimal> suministros = q.getResultList();

        return suministros;
    }
}
