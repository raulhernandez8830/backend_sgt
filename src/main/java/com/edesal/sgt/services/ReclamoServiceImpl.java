package com.edesal.sgt.services;

import com.edesal.sgt.dao.ReclamoDAO;
import com.edesal.sgt.models.Reclamo;
import org.hibernate.engine.spi.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by DHernandez on 17/9/2019.
 */
@Service
public class ReclamoServiceImpl implements ReclamoService {

    @Autowired
    EntityManager em;

    @Autowired
    ReclamoDAO reclamodao;

    @Override
    public List<Reclamo> getReclamos() {
        Query q = em.createQuery("SELECT r FROM Reclamo r WHERE r.codigo_mreclamo='FE' OR r.codigo_mreclamo='OC'     ORDER BY r.fecha");

        List<Reclamo> reclamos = q.getResultList();

        return reclamos;
    }
}
