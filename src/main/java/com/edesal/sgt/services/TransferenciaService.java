package com.edesal.sgt.services;

import com.edesal.sgt.models.RedElectrica;
import com.edesal.sgt.models.TipoTransferencia;
import com.edesal.sgt.models.Transferencia;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by DHernandez on 5/9/2019.
 */
public interface TransferenciaService {

    public Transferencia saveTransferencia(Transferencia transferencia);

    // listar los tipos de transferencias
    public List<TipoTransferencia> getTipoTransferencias();

    // listamos una tabla de transferencias segun opcion enviada al ws
    public List<RedElectrica> getTransferencia(String tipo);

    // obtener la informacion de un tipo de transferencia por su numero de id
    public TipoTransferencia getTrasnferencia(Long id);

}
