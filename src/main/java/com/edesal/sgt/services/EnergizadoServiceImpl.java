package com.edesal.sgt.services;

import com.edesal.sgt.dao.EnergizadoDAO;
import com.edesal.sgt.models.Energizado;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 * Created by DHernandez on 7/10/2019.
 */
@Service
public class EnergizadoServiceImpl implements EnergizadoService {

    @Autowired
    EntityManager em;

    @Autowired
    EnergizadoDAO energizadodao;


    // obtener las com.edesal.sgt.configuraciones de los cortes energizados segun corte aperturado y cerrado
    @Override
    public Energizado getEnergizadosByConfiguracion(String corteaperturado, String cortecerrado) {
        Query q = em.createQuery("SELECT e FROM Energizado e WHERE e.corte_aperturado =: aperturado AND e.corte_cerrado =: cerrado");
        q.setParameter("aperturado", corteaperturado);
        q.setParameter("cerrado", cortecerrado);

        Energizado e = (Energizado) q.getSingleResult();

        return e;
    }


}
