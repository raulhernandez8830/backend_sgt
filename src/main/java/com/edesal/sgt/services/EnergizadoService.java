package com.edesal.sgt.services;

import com.edesal.sgt.models.Energizado;

/**
 * Created by DHernandez on 7/10/2019.
 */
public interface EnergizadoService {

    // obtener las com.edesal.sgt.configuraciones de los cortes energizados segun cortes seleccionados
    Energizado getEnergizadosByConfiguracion(String corteaperturado, String cortecerrado);

}
