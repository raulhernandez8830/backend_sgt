package com.edesal.sgt.services;

import java.util.List;

/**
 * Created by DHernandez on 1/10/2019.
 */
public interface CorteMTService {

    // obtener elementos para cierre segun opcion principal o secundario
    public List<String> getElementosCierrePS(String elemento, String opcion);


    // obtener los elementos de tipo principal
    public List<String> getElementosPrincipales(String corte, String opcion);

}
