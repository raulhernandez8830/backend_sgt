package com.edesal.sgt.services;

import com.edesal.sgt.models.DetalleManiobra;
import com.edesal.sgt.models.Maniobra;
import org.omg.CORBA.portable.ApplicationException;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by DHernandez on 23/8/2019.
 */
public interface ManiobraService {

    //obtener todas las maniobras almacenadas en la db
    public List<Maniobra> getManiobras();

    //guardar una maniobra con sus detalles
    public Maniobra saveManiobra(Maniobra maniobra);

    // update maniobra (ESTADO)
    public Maniobra validarManiobra(Maniobra maniobra);

    // actualizar estado de maniobra asociada
    public Integer setManiobraAsociada(Long id);

    // comprobamos si un elemento pertence a una cadena de red segun el corte padre
    public String elementoRed(String padre, String hijo);

    // editar una linea de itinerario
    public DetalleManiobra updateItinerario(DetalleManiobra itinerario);

}
