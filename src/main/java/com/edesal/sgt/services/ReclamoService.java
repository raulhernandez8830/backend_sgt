package com.edesal.sgt.services;

import com.edesal.sgt.models.Reclamo;

import java.util.List;

/**
 * Created by DHernandez on 17/9/2019.
 */
public interface ReclamoService {

    // listar los reclamos existentes en la db
    public List<Reclamo> getReclamos();
}
