package com.edesal.sgt.services;

import com.edesal.sgt.dao.CorteMTPDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by DHernandez on 1/10/2019.
 */
@Service
public class CorteMTServiceImpl implements CorteMTService {

    @Autowired
    EntityManager em;

    @Autowired
    CorteMTPDAO cortemtdao;

    @Override
    public List<String> getElementosCierrePS(String elemento, String opcion) {

        List<String> cortes = new ArrayList<>();

        switch (opcion) {
            case "P":
                Query q = em.createNativeQuery("SELECT cierre_principal FROM SGT_cortes_mt_principal WHERE elemento_aperturado =:elemento ");
                q.setParameter("elemento", elemento);
                List<String> cortesp = q.getResultList();

                return cortesp;

            case "S":
                Query q1 = em.createNativeQuery("SELECT cierre_secundario FROM SGT_cortes_mt_secundario WHERE elemento_aperturado =:elemento ");
                q1.setParameter("elemento", elemento);
                List<String> cortes1 = q1.getResultList();

                return cortes1;

        }

        return cortes;




    }


    @Override
    public List<String> getElementosPrincipales(String corte, String opcion) {
        List<String> cortes = new ArrayList<>();

        switch (opcion) {
            case "P":
                Query q = em.createNativeQuery("SELECT cierre_principal FROM SGT_cortes_mt_principal WHERE elemento_aperturado = :elemento ");
                q.setParameter("elemento", corte);
                List<String> cortesp = q.getResultList();


                return cortesp;

            case "S":
                Query q1 = em.createNativeQuery("SELECT cierre_secundario FROM SGT_cortes_mt_secundario WHERE elemento_aperturado == :elemento ");
                q1.setParameter("elemento", corte);
                List<String> cortes1 = q1.getResultList();

                return cortes1;

        }

        return cortes;

    }
}
