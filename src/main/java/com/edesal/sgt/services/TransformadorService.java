package com.edesal.sgt.services;

import com.edesal.sgt.models.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by DHernandez on 15/5/2020.
 */
public interface TransformadorService {

    // obtener todos los transformadores
    public List<Transformador> getTrafos();

    // obtener el poste que corresponde al transformador
    public  List<Poste> getPosteByTrafo(String poste);

    // obtener el circuito que corresponde al transformador
    public Circuito getCircuitoByTrafo(String circuito);

    // obtener los circuitos listados en db
    public List<Circuito> getAllCircuitos();

    // guardar un trafo
    public Transformador saveTrafo(Transformador trafo);

    // obtener listado de postes
    public List<String> getPostes();


    // obtener los voltajes necesarios para trafos
    public List<Voltaje> getVoltajes();

    // obtener los materiales de estructura para la creacion de un nuevo trafo
    public List<MaterialesEstructura> getEsctructurasMateriales();


    // Update trafo
    public Transformador update(Transformador transformador);

    // proceso de baja transformador
    public Transformador bajaTrafo(Transformador transformador);

    // usuarios activos de transformador
    public List<BigDecimal> suministrosActivosTrafo(String codigo);

}
