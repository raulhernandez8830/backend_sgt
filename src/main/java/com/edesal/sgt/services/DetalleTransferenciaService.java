package com.edesal.sgt.services;

import com.edesal.sgt.models.DetalleTransferencia;

/**
 * Created by DHernandez on 5/9/2019.
 */
public interface DetalleTransferenciaService {

    // metodo para guardar los detalles de la transferencia
    public DetalleTransferencia saveDetalleTransferencia(DetalleTransferencia detalle);
}
