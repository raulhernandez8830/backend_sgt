package com.edesal.sgt.services;

import com.edesal.sgt.models.Usuario;

import java.util.List;

/**
 * Created by DHernandez on 19/8/2019.
 */
public interface UsuarioService {

    //metodo para listar los usuarios almacenados en la db
    public List<Usuario> getUsuarios();

    //metodo para validar credenciales
    public Usuario validarCredenciales(Usuario usuario);
}
