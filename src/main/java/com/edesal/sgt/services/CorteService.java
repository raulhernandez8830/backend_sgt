package com.edesal.sgt.services;

import com.edesal.sgt.models.Corte;
import com.edesal.sgt.models.RedElectrica;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by DHernandez on 4/9/2019.
 */
public interface CorteService {

    // busqueda de un corte por su codigo de corte
    public BigDecimal getCorteByCodigo(String codigo);

    // obtener todos los elementos
    public List<String> getAllElementos();

    // obtener todos los cortes de la red electrica padre e hijo
    public List<RedElectrica> getRedElectrica();

    // obtener los cortes que pertenecen a maniobra de transferencia para los aperturados
    public List<String> getElementosAperturadosMT();

    // buscar elemento aperturado para verificar si tiene activada la bandera de mt
    public Corte buscarElementoAperturado(String elemento);

    // obtener elementos por circuito
    public List<String> getElementosByCircuito(String trafo);

    // obtener los cortes de la ENS_CORTES
    public List<Corte> getCortes();

    // obtener un corte por su id
    public Corte getCorteById(String trafo);

    // obtener un elemento de tipo corte por su codigo
    public Corte obtenerCorteXCodigo(String codigo);

    // obtener todos los cortes
    public List<Corte> getCortesAll();


}
