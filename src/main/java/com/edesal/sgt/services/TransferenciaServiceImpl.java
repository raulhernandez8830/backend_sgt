package com.edesal.sgt.services;

import com.edesal.sgt.dao.DetalleTransferenciaDAO;
import com.edesal.sgt.dao.TransferenciaDAO;
import com.edesal.sgt.models.DetalleTransferencia;
import com.edesal.sgt.models.RedElectrica;
import com.edesal.sgt.models.TipoTransferencia;
import com.edesal.sgt.models.Transferencia;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by DHernandez on 5/9/2019.
 */
@Service
public class TransferenciaServiceImpl implements TransferenciaService {

    @Autowired
    EntityManager em;

    @Autowired
    TransferenciaDAO transferenciadao;

    @Autowired
    DetalleTransferenciaDAO detalletransferenciadao;

    @Override
    public Transferencia saveTransferencia(Transferencia transferencia) {

        Transferencia transferenciadto = transferenciadao.save(transferencia);


        if(transferenciadto.getDetallestransferencia()!=null) {
            for(int i=0; i<transferenciadto.getDetallestransferencia().size(); i++) {
                DetalleTransferencia detalletransferencia = new DetalleTransferencia();
                detalletransferencia = transferenciadto.getDetallestransferencia().get(i);
                detalletransferencia.setTransferencia(transferenciadto);

                // persistimos los detalles de la transferencia
                detalletransferenciadao.save(detalletransferencia);
            }
        }


        return transferenciadto;
    }

    // obtener los tipos de transferencias
    @Override
    public List<TipoTransferencia> getTipoTransferencias() {

        Query q = em.createQuery("SELECT t FROM TipoTransferencia t ORDER BY t.nombre ASC");
        List<TipoTransferencia> tipos = q.getResultList();

        return tipos;
    }

    @Override
    public List<RedElectrica> getTransferencia(String tipo) {

        return null;
    }

    @Override
    public TipoTransferencia getTrasnferencia(Long id) {
        Query q = em.createQuery("SELECT t FROM TipoTransferencia t WHERE t.id =: id ");
        q.setParameter("id", id);

        TipoTransferencia transferencia = (TipoTransferencia) q.getSingleResult();

        return transferencia;
    }


}
