package com.edesal.sgt.services;

import com.edesal.sgt.dao.UsuarioDAO;
import com.edesal.sgt.models.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by DHernandez on 19/8/2019.
 */

@Service
public class UsuarioServiceImpl implements UsuarioService {


    @Autowired
    UsuarioDAO usuariodao;

    @PersistenceContext
    EntityManager em;


    @Override
    public List<Usuario> getUsuarios() {
        Query q = em.createQuery("SELECT u FROM Usuario u ");

        List<Usuario> usuarios = q.getResultList();

        return usuarios;
    }

    @Override
    public Usuario validarCredenciales(Usuario usuario) {
        Query q = em.createQuery("SELECT u FROM Usuario u WHERE u.alias=:u AND u.password=:p");
        q.setParameter("u",usuario.getAlias().toString());
        q.setParameter("p",usuario.getPassword().toString());
        Usuario u = (Usuario) q.getSingleResult();

        return u;
    }
}
