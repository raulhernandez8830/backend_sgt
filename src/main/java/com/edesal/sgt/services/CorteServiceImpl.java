package com.edesal.sgt.services;

import com.edesal.sgt.dao.CorteDAO;
import com.edesal.sgt.models.Corte;
import com.edesal.sgt.models.RedElectrica;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by DHernandez on 4/9/2019.
 */
@Service
public class CorteServiceImpl implements CorteService {

    @Autowired
    CorteDAO cortedao;

    @Autowired
    EntityManager em;

    @Override
    public BigDecimal getCorteByCodigo(String codigo) {

        String inicial = codigo.substring(0,1);

        System.out.println(inicial);

        BigDecimal elemento = new BigDecimal(5);

        if(inicial == "T"){
            Query q1 = em.createNativeQuery("SELECT TOP 1 SEC_TRAFO FROM ENS_TRANSFORMADORES  WHERE CODIGO_TRAFO=?");
            q1.setParameter(1,codigo);
            BigDecimal trafo = (BigDecimal) q1.getSingleResult();
            return trafo;
        } else if(inicial == "C"){
            Query q = em.createNativeQuery("SELECT TOP 1 SEC_CORTE FROM ENS_CORTES  WHERE CODIGO_CORTE=?");
            q.setParameter(1,codigo);
            BigDecimal corte = (BigDecimal) q.getSingleResult();
            return corte;
        }

        return elemento;
    }

    @Override
    public List<String> getAllElementos() {
        Query q = em.createNativeQuery("SELECT CODIGO_TRAFO FROM ENS_TRANSFORMADORES UNION SELECT CODIGO_CORTE collate SQL_Latin1_General_CP1_CI_AS FROM ENS_CORTES");

        List<String> elementos = q.getResultList();
        elementos.add("ACOMETIDA");

        return elementos;
    }

    @Override
    public List<RedElectrica> getRedElectrica() {

        List<Object[]> corteslist = new ArrayList<>();
        List<RedElectrica> cortesdtolist = new ArrayList<>();


        List<Object[]> cortes =  em.createNativeQuery("SELECT c.CODIGO_CORTE as padre, p.CODIGO_CORTE as hijo FROM ENS_CORTES c\n" +
                "INNER JOIN ENS_CORTES p ON p.corte_padre = c.SEC_CORTE\n" +
                "ORDER BY c.CODIGO_CORTE").getResultList();


        for(Object[] corte : cortes){
            RedElectrica cortedto = new RedElectrica();

            String padre = (String) corte[0];
            String hijo  = (String) corte[1];

            cortedto.setPadre(padre);
            cortedto.setHijo(hijo);

            cortesdtolist.add(cortedto);
        }

        return cortesdtolist;
    }

    @Override
    public List<String> getElementosAperturadosMT() {
        Query q = em.createNativeQuery("SELECT c.codigo_corte FROM ENS_CORTES c WHERE c.corte_mt_ap = 1");

        List<String> cortes = q.getResultList();

        return cortes;
    }

    @Override
    public Corte buscarElementoAperturado(String elemento) {
        Query q = em.createQuery("SELECT c FROM Corte c WHERE c.codigo_corte =:elemento");
        q.setParameter("elemento",elemento);

        Corte corte = (Corte) q.getSingleResult();

        return corte;
    }

    @Override
    public List<String> getElementosByCircuito(String trafo) {

        List<String> elementos = new ArrayList<>();

        StoredProcedureQuery sp = em.createStoredProcedureQuery("sp_getCortesXCircuito");
        sp.registerStoredProcedureParameter("trafo",String.class, ParameterMode.IN);
        sp.setParameter("trafo",trafo);
        sp.execute();

        Query q = em.createNativeQuery("SELECT TOP 1 NIVEL FROM temp_circuito ORDER BY nivel DESC ");
        int nivel = (int) q.getSingleResult();



        for(Integer i=0; i<nivel; i++){
            Query q1 = em.createNativeQuery("SELECT TOP 1 padre FROM temp_circuito ORDER BY nivel ASC ");
            BigDecimal corte = (BigDecimal) q1.getSingleResult();

            if(corte!=null){
                StoredProcedureQuery sp1 = em.createStoredProcedureQuery("sp_getCortesByCircuitoRecorrido");
                sp1.registerStoredProcedureParameter("corte",BigDecimal.class, ParameterMode.IN);
                sp1.setParameter("corte",corte);
                sp1.execute();
            }

        }

        // listamos los elementos almacenados del circuito en un arreglo de elementos de cortes
        Query qelementos = em.createNativeQuery("SELECT codigo_corte FROM temp_circuito");
        elementos = qelementos.getResultList();

        return elementos;
    }

    @Override
    public List<Corte> getCortes() {
        Query q = em.createQuery("SELECT c FROM Corte c");
        List<Corte> cortes = q.getResultList();

        return cortes;
    }


    @Override
    public Corte getCorteById(String trafo) {

      Query q = em.createNativeQuery("SELECT c.CODIGO_CORTE FROM ENS_CORTES c\n" +
              "INNER JOIN ENS_TRANSFORMADORES E ON c.SEC_CORTE = E.SEC_CORTE\n" +
              "WHERE E.CODIGO_TRAFO=:t");
      q.setParameter("t",trafo);
      String c_  = (String) q.getSingleResult();

      Corte cortedto = new Corte();
      cortedto.setCodigo_corte(c_);

      return cortedto;
    }


    @Override
    public Corte obtenerCorteXCodigo(String codigo) {
        Query q = em.createQuery("SELECT c FROM Corte c WHERE c.codigo_corte=:c1");
        q.setParameter("c1",codigo);

        Corte cortedto = new Corte();

        Object result = null;
        try {
            cortedto = (Corte) q.getSingleResult();
        } catch (NoResultException e) {
            System.out.println("No se encontraron resultados para esta consulta... " + e.getMessage());
        }

        return cortedto;
    }

    @Override
    public List<Corte> getCortesAll() {
        Query q = em.createQuery("SELECT c FROM Corte c");
        List<Corte> cortes = q.getResultList();

        return cortes;
    }

}
