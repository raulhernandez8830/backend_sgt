package com.edesal.sgt.services;

import com.edesal.sgt.dao.*;
import com.edesal.sgt.models.*;
import com.edesal.sgt.models.SIGET.CompensacionSIGET;
import com.edesal.sgt.models.SIGET.DatosCentroSIGET;
import com.edesal.sgt.models.SIGET.InstalacionSIGET;
import com.edesal.sgt.models.SIGET.ReclamoInterSIGET;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import javax.transaction.Transaction;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by DHernandez on 21/8/2019.
 */
@Service
public class InterrupcionServiceImpl implements InterrupcionService {

    @Autowired
    InterrupcionDAO interrupciondao;


    public Interrupcion interrupcion_global = new Interrupcion();

    @PersistenceContext
    EntityManager em;

    @Autowired
    DetalleInterrupcionDAO detalleinterrupciondao;

    @Autowired
    ReclamoDAO reclamodao;

    @Autowired
    DetalleManiobraDAO detallemaniobradao;

    @Autowired
    ManiobraDAO maniobradao;

    @Override
    public List<Interrupcion> getInterrupciones() {

        Query q = em.createQuery("SELECT i FROM Interrupcion i WHERE i.estado='G' " +
                "AND FUNCTION('YEAR',fecha_ingreso)>=2019 ORDER BY ID_INTERRUPCION DESC ");


        List<Interrupcion> interrupciones = q.getResultList();

        return interrupciones;

    }

    @Override
    public List<Object> getElementosBySistema(String sistema) {
        List<Object> elementos = new ArrayList<>();

        switch (sistema)
        {
            case "BT":
                Query q = em.createNativeQuery("SELECT CODIGO_TRAFO FROM ENS_TRANSFORMADORES");
                List<Object> transformadores = q.getResultList();
                return transformadores;

            case "MT":
                Query q1 = em.createNativeQuery("SELECT CODIGO_CORTE FROM ENS_CORTES");
                List<Object> cortes = q1.getResultList();
                return cortes;

                default:
                    return elementos;
        }




    }

    @Override
    public List<Causa> getCausasInterrupcion() {
        Query q = em.createQuery("SELECT c FROM Causa c");
        List<Causa> causas = q.getResultList();

        return causas;
    }

    @Override
    @Transactional
    public Interrupcion saveInterrupcion(Interrupcion interrupcion) {

      try {
          List<DetalleInterrupcion> detalleinterrupcion = new ArrayList<>();
          detalleinterrupcion = interrupcion.getDetallemaniobra();




          String correlativo_db = getCorrelativoByTension(interrupcion.getPeriodo());
          Integer correlativo = Integer.parseInt(correlativo_db) + 1;


          Query qcor = em.createNativeQuery("SELECT SECUENCIAL FROM FE_SECUENCIAL WHERE CODIGO_SECUENCIA=8");
          BigDecimal id_secuencial = (BigDecimal) qcor.getSingleResult();

          String elemento = interrupcion.getElemento();
          String calificacion = interrupcion.getCalificacion();


          //establecemos el codigo de la interrupcion

              interrupcion.setCodigo_interrupcion(interrupcion.getTipo_sistema().substring(0, 1) + interrupcion.getElemento().substring(0, 1) + interrupcion.getPeriodo() + "-" + String.valueOf(correlativo));

          //set estado de la interrupcion en G
          interrupcion.setEstado("G");


          // el tipo de tension es igual tipo sistema
          interrupcion.setTipo_tension(interrupcion.getTipo_sistema());

          if(interrupcion.getAcometidad() == "S") {
              interrupcion.setAcometidad("1");
          } else {
              interrupcion.setAcometidad("0");
          }



          // establecemos el correlativo
          interrupcion.setCorrelativo(correlativo.longValue());

          // consulta para traer el maximo id de la interrupcion
          Query q = em.createQuery("SELECT MAX(id) FROM Interrupcion");
          Object max_interr = q.getSingleResult();




          Interrupcion interrupciondto = new Interrupcion();

          // Establecemos el estado en G para las interrupciones generadas
          interrupcion.setEstado("G");

          // establecemos id de interru
          interrupcion.setId(id_secuencial.longValue());

          // System.out.println(interrupcion.getId_interrupcion() + "  consulta: "+ max_interr );


          if(detalleinterrupcion.size() > 0) {
              for(int i=0; i<detalleinterrupcion.size(); i++){
                  DetalleManiobra detalleitinerario = new DetalleManiobra();
                  Long id_ = detalleinterrupcion.get(i).getItinerario();
                  Query q1 = em.createQuery("SELECT d FROM DetalleManiobra d  WHERE d.id=:id_");
                  q1.setParameter("id_",id_.longValue());
                  detalleitinerario = (DetalleManiobra) q1.getSingleResult();

                  if(detalleitinerario.getGenera_reposicion() == 1) {
                      interrupcion.setCodigo_reposicion(interrupcion.getCodigo_interrupcion() + '-' + correlativo);
                  }
              }
          }



          // persistimos el objeto interrupcion
          interrupciondto = interrupciondao.save(interrupcion);
          this.interrupcion_global = interrupciondto;

         try {

             Long nuevo_secuencial = id_secuencial.longValue() + 1;
             Query qupdatesecuencial = em.createQuery("UPDATE Secuencial s SET s.secuencial=:n WHERE s.codigo_secuencia=8 ");
             qupdatesecuencial.setParameter("n", nuevo_secuencial );
             int u = qupdatesecuencial.executeUpdate();
         }catch(TransactionRequiredException t) {
             System.out.println(t.getMessage());
         }

         Interrupcion inter = new Interrupcion();


          Long id = interrupciondto.getId();
          inter = interrupciondao.findTopById(Long.valueOf(id));


          // Proceso si existe un detalle de maniobar de lo contrario se considera como una interrupcion no procedente




                if(detalleinterrupcion.size() > 0) {

                    // proceso para la generacion de codigos de reposicion

                    DetalleManiobra dt_m = detallemaniobradao.findTopById(interrupciondto.getDetallemaniobra().get(0).getItinerario());


                    // conteo lineas por maniobra
                    int conteo_lineas = detallemaniobradao.countByManiobra(dt_m.getManiobra());

                    //recorremos un for para guardar los detalles de la interrupcion
                    if (interrupcion.getDetallemaniobra().size() > 0) {
                        correlativo = 1;
                        for (Integer i = 0; i < interrupciondto.getDetallemaniobra().size(); i++) {

                            DetalleInterrupcion detalle = new DetalleInterrupcion();
                            detalle = interrupciondto.getDetallemaniobra().get(i);
                            detalle.setInterrupcion_(interrupciondto);

                            if (dt_m.getGenera_reposicion() == 1) {
                                detalle.setCodigo_reposicion(interrupcion.getCodigo_interrupcion() + '-' + correlativo);
                            }
                            // persistimos el detalle de la interrupcion
                            detalleinterrupciondao.save(detalle);

                            // actualizamos la linea de itinerario
                            DetalleManiobra dtmaniobra = new DetalleManiobra();
                            dtmaniobra = detallemaniobradao.findTopById(interrupciondto.getDetallemaniobra().get(i).getItinerario());
                            dtmaniobra.setAsociada(1);

                            detallemaniobradao.save(dtmaniobra);

                            correlativo += 1;

                        }

                        // conteo de asociaciones por maniobra
                        // Integer conteo_asociadas = detallemaniobradao.countByManiobraAndAsociada(dt_m.getManiobra(), 1);
                        Query qw = em.createNativeQuery("SELECT COUNT(dt.id) FROM SGT_detalles_maniobra dt WHERE dt.maniobra_id=:m AND asociada=1");
                        qw.setParameter("m", dt_m.getManiobra().getId());
                        Object o = qw.getSingleResult();

                        System.out.println(conteo_lineas + " ** " + o);

                        if (conteo_lineas == (Integer) o) {
                            Maniobra m = maniobradao.getManiobraById(dt_m.getManiobra().getId());
                            m.setEstado(3);
                            maniobradao.save(m);
                        }
                    }


                }



          // recorremos con un for el arreglo de reclamos si la interrupcion es de tipo asociacion
          if(interrupcion.getReclamos().size() > 0) {
              for (Integer i = 0; i < interrupcion.getReclamos().size(); i++) {
                  Reclamo reclamodto = reclamodao.findTopByCorrelativo(interrupcion.getReclamos().get(i).getCorrelativo());
                  reclamodto.setInterrupcion(interrupcion);

                  //persistimos el reclamo para actualizar su informacion
                  reclamodao.save(reclamodto);
              }
          }



          return inter;

      }catch(NoResultException e) {
          System.out.println(e.getMessage());
          Interrupcion in = new Interrupcion();
          return in;
      }



    }



    @Override
    public String getCorrelativoByTension(String periodo) {
        Query q1 = em.createNativeQuery("SELECT COUNT(i.CODIGO_INTERRUPCION) FROM ENS_INTERRUPCION i WHERE i.PERIODO = :p ");
        q1.setParameter("p",periodo);
        Object queryrun = q1.getSingleResult();

        return queryrun.toString();
    }

    @Override
    public List<Interrupcion> interrupcionesCerradas() {
        Query q = em.createQuery("SELECT i FROM Interrupcion i WHERE i.estado='C' " +
                "AND FUNCTION('YEAR',fecha_ingreso)>=2019 ORDER BY fecha_ingreso DESC ");


        List<Interrupcion> interrupciones = q.getResultList();

        return  interrupciones;
    }

    public List<UsuarioAfectado> getUsuariosAfectados(String periodo) {

        StoredProcedureQuery sp = em.createStoredProcedureQuery("sp_usuarios_afectados");
        sp.registerStoredProcedureParameter("periodo",String.class, ParameterMode.IN);
        sp.setParameter("periodo",periodo);
        sp.execute();

        Query q = em.createQuery("SELECT u FROM UsuarioAfectado u");

        List<UsuarioAfectado> usuarioafectados = q.getResultList();

        return  usuarioafectados;

    }

    @Override
    public Causa getCausa(String nombre) {

        return null;
    }

    @Override
    public Interrupcion updateInterrupcion(Interrupcion interrupcion) {

        List<DetalleInterrupcion> detalleinterrupcion = new ArrayList<>();
        detalleinterrupcion = interrupcion.getDetallemaniobra();

        interrupcion.setDetallemaniobra(new ArrayList<>());

        interrupcion.setCodigo_interrupcion(interrupcion.getTipo_sistema().substring(0, 1) + interrupcion.getElemento().substring(0, 1) + interrupcion.getPeriodo() + "-" + String.valueOf(interrupcion.getCorrelativo()));

        interrupcion.setCodigo_reposicion(interrupcion.getCodigo_interrupcion() + '-' + interrupcion.getCorrelativo());

        interrupcion.setTipo_tension(interrupcion.getTipo_sistema());



        Interrupcion interrupciondto = interrupciondao.findTopById(interrupcion.getId());



        interrupciondto.setReclamos(interrupcion.getReclamos());



        // persistimos el objeto interrupcion
        interrupciondto = interrupciondao.save(interrupcion);



        // Proceso de asociacion de itinerario a una interrupcion
        if(detalleinterrupcion.size() > 0) {

            // proceso para la generacion de codigos de reposicion

            DetalleManiobra dt_m = detallemaniobradao.findTopById(detalleinterrupcion.get(0).getItinerario());


            // conteo lineas por maniobra
            int conteo_lineas = detallemaniobradao.countByManiobra(dt_m.getManiobra());

            //recorremos un for para guardar los detalles de la interrupcion
            if (detalleinterrupcion.size() > 0) {

                for (Integer i = 0; i < detalleinterrupcion.size(); i++) {


                    DetalleInterrupcion detalle = new DetalleInterrupcion();
                    detalle.setInterrupcion_(interrupcion);
                    detalle.setItinerario(detalleinterrupcion.get(i).getItinerario());

                    if (dt_m.getGenera_reposicion() == 1) {
                        detalle.setCodigo_reposicion(interrupcion.getCodigo_interrupcion() + '-' + interrupcion.getCorrelativo());
                    }
                    // persistimos el detalle de la interrupcion
                    detalleinterrupciondao.save(detalle);

                    // actualizamos la linea de itinerario
                    DetalleManiobra dtmaniobra = new DetalleManiobra();
                    dtmaniobra = detallemaniobradao.findTopById(detalleinterrupcion.get(i).getItinerario());
                    dtmaniobra.setAsociada(1);

                    detallemaniobradao.save(dtmaniobra);

                    int cont = 0;
                    System.out.println("iteracion: " + cont);
                    cont++;

                }

                // conteo de asociaciones por maniobra
                // Integer conteo_asociadas = detallemaniobradao.countByManiobraAndAsociada(dt_m.getManiobra(), 1);
                Query qw = em.createNativeQuery("SELECT COUNT(dt.id) FROM SGT_detalles_maniobra dt WHERE dt.maniobra_id=:m AND asociada=1");
                qw.setParameter("m", dt_m.getManiobra().getId());
                Object o = qw.getSingleResult();

                System.out.println(conteo_lineas + " ** " + o);

                if (conteo_lineas == (Integer) o) {
                    Maniobra m = maniobradao.getManiobraById(dt_m.getManiobra().getId());
                    m.setEstado(3);
                    maniobradao.save(m);
                }
            }


        }







        return interrupciondto;
    }

    @Override
    public Interrupcion getInterrupcionByReclamo() {
        Query q = em.createNativeQuery("SELECT i.ID_INTERRUPCION FROM ENS_INTERRUPCION_SUMINISTRO en\n" +
                "INNER JOIN fe_calidad_reclamos fcr ON en.CORRELATIVO = fcr.correlativo\n" +
                "INNER JOIN ENS_INTERRUPCION i ON i.ID_INTERRUPCION = en.ID_INTERUPCION\n" +
                "WHERE fcr.correlativo = 7697");

        Interrupcion i = (Interrupcion) q.getSingleResult();

        return i;
    }

    @Override
    public Integer getConteoNIS() {
        Query q= em.createNativeQuery(" SELECT COUNT( num_suministro) as suministros FROM temp_ens");
        Integer conteonis = (Integer) q.getSingleResult();

        return conteonis;

    }

    @Override
    public Integer getConteoTrafos() {
        Query q= em.createNativeQuery(" SELECT COUNT(DISTINCT trafo) FROM temp_ens");
        Integer conteotrafos = (Integer) q.getSingleResult();

        return conteotrafos;
    }

    @Override
    public List<InterrupcionSuministro> getSuministrosAfectadosByInterrup(){
        Query q= em.createNativeQuery("SELECT codigo_interrupcion , COUNT( num_suministro) AS conteo FROM temp_ens GROUP BY codigo_interrupcion\n");
        List<Object[]> conteos = q.getResultList();

        List<InterrupcionSuministro> listaconteo = new ArrayList<>();

        for (Object[] a : conteos) {
            InterrupcionSuministro conteodto = new InterrupcionSuministro();
            conteodto.setInterrupcion((String) a[0]);
            conteodto.setConteo((Integer) a[1]);

            listaconteo.add(conteodto);

        }

        return listaconteo;

    }

    @Override
    public List<InterrupcionSuministro> getTrafoAfectadosByInterrup() {
        Query q= em.createNativeQuery("SELECT codigo_interrupcion , COUNT(DISTINCT trafo) AS conteo FROM temp_ens GROUP BY codigo_interrupcion\n");
        List<Object[]> conteos = q.getResultList();

        List<InterrupcionSuministro> listaconteo = new ArrayList<>();

        for (Object[] a : conteos) {
            InterrupcionSuministro conteodto = new InterrupcionSuministro();
            conteodto.setInterrupcion((String) a[0]);
            conteodto.setConteo((Integer) a[1]);

            listaconteo.add(conteodto);

        }

        return listaconteo;
    }

    @Override
    public List<InterrupcionSIGET> tbl_interrupcionesSIGET(String periodo) {
        StoredProcedureQuery sp = em.createStoredProcedureQuery("sp_interrupcionesSIGET");
        sp.registerStoredProcedureParameter("periodo",String.class,ParameterMode.IN);
        sp.setParameter("periodo", periodo);
        sp.execute();

        // listamos los resultados al ejecturar el store procedure
        Query q = em.createQuery("SELECT i FROM InterrupcionSIGET i");
        List<InterrupcionSIGET> interrupciones = q.getResultList();


        return interrupciones;

    }

    @Override
    public List<ReposicionSIGET> tbl_reposicionesSIGET(String periodo) {
        StoredProcedureQuery sp = em.createStoredProcedureQuery("sp_reposicionesSIGET");
        sp.registerStoredProcedureParameter("periodo",String.class,ParameterMode.IN);
        sp.setParameter("periodo", periodo);
        sp.execute();

        // listamos los resultados al ejecturar el store procedure
        Query q = em.createQuery("SELECT r FROM ReposicionSIGET r");
        List<ReposicionSIGET> reposiciones = q.getResultList();


        return reposiciones;
    }

    @Override
    public List<InterrupcionExternaSIGET> tbl_externasSIGET(String periodo) {

        StoredProcedureQuery sp = em.createStoredProcedureQuery("sp_interrupcionesExternasSIGET");
        sp.registerStoredProcedureParameter("periodo", String.class, ParameterMode.IN);
        sp.setParameter("periodo", periodo);
        sp.execute();

        Query q = em.createQuery("SELECT e FROM InterrupcionExternaSIGET e");
        List<InterrupcionExternaSIGET> e = q.getResultList();

        return e;
    }

    @Override
    public List<CentroMTBT> tbl_centrosmtbt(String periodo) {
        StoredProcedureQuery sp = em.createStoredProcedureQuery("sp_centrosMTBT");
        sp.registerStoredProcedureParameter("periodo", String.class, ParameterMode.IN);
        sp.setParameter("periodo", periodo);
        sp.execute();

        Query q = em.createQuery("SELECT e FROM CentroMTBT e");
        List<CentroMTBT> e = q.getResultList();

        return e;
    }

    @Override
    public List<RepUsuarioSIGET> tbl_repusuarios(String periodo) {
        StoredProcedureQuery sp = em.createStoredProcedureQuery("sp_repusuariosSIGET");
        sp.registerStoredProcedureParameter("periodo", String.class, ParameterMode.IN);
        sp.setParameter("periodo", periodo);
        sp.execute();

        Query q = em.createQuery("SELECT e FROM RepUsuarioSIGET e");
        List<RepUsuarioSIGET> e = q.getResultList();

        return e;
    }


    @Override
    public List<ReclamoInterSIGET> tbl_reclamosinterr(String periodo) {
        StoredProcedureQuery sp = em.createStoredProcedureQuery("sp_reclamosinterrSIGET");
        sp.registerStoredProcedureParameter("periodo", String.class, ParameterMode.IN);
        sp.setParameter("periodo", periodo);
        sp.execute();

        Query q = em.createQuery("SELECT ri FROM ReclamoInterSIGET ri");
        List<ReclamoInterSIGET> r = q.getResultList();

        return r;
    }


    @Override
    public List<DatosCentroSIGET> tbl_datoscentros(String periodo) {
        StoredProcedureQuery sp = em.createStoredProcedureQuery("sp_datoscentrosSIGET");
        sp.registerStoredProcedureParameter("periodo", String.class, ParameterMode.IN);
        sp.setParameter("periodo", periodo);
        sp.execute();

        Query r = em.createQuery("SELECT d FROM DatosCentroSIGET d");
        List<DatosCentroSIGET> d = r.getResultList();

        return d;
    }

    @Override
    public List<InstalacionSIGET> tbl_instalacionessiget(String periodo) {
        StoredProcedureQuery sp = em.createStoredProcedureQuery("sp_instalacionesSIGET");
        sp.registerStoredProcedureParameter("periodo", String.class, ParameterMode.IN);
        sp.setParameter("periodo", periodo);
        sp.execute();

        Query r = em.createQuery("SELECT d FROM InstalacionSIGET d");
        List<InstalacionSIGET> d = r.getResultList();

        return d;
    }


    @Override
    public List<CompensacionSIGET> tbl_compensacionessiget(String periodo) {
        StoredProcedureQuery sp = em.createStoredProcedureQuery("sp_compensacionesSIGET");
        sp.registerStoredProcedureParameter("periodo", String.class, ParameterMode.IN);
        sp.setParameter("periodo", periodo);
        sp.execute();

        Query r = em.createQuery("SELECT d FROM CompensacionSIGET d");
        List<CompensacionSIGET> d = r.getResultList();

        return d;
    }

    @Override
    public Interrupcion finalizarInterrupcion(Interrupcion interrupcion) {
        interrupcion.setEstado("C");

        try{
            Interrupcion interrupcion_dto = interrupciondao.findTopById(interrupcion.getId());
            interrupcion_dto = interrupcion;

            //persistimos
            interrupciondao.save(interrupcion_dto);

            Query q = em.createQuery("SELECT i FROM Interrupcion i WHERE i.id =:id_");
            q.setParameter("id_", interrupcion.getId());
            Interrupcion _interr = (Interrupcion) q.getSingleResult();

            for(Integer i=0; i<interrupcion.getDetallemaniobra().size(); i++) {
                DetalleInterrupcion detalle = new DetalleInterrupcion();
                detalle = interrupcion.getDetallemaniobra().get(i);
                detalle.setInterrupcion_(interrupcion);
                detalleinterrupciondao.save(detalle);

            }

            // recorremos con un for el arreglo de reclamos si la interrupcion es de tipo asociacion
            if(interrupcion.getReclamos().size() > 0) {
                for (Integer i = 0; i < interrupcion.getReclamos().size(); i++) {
                    Reclamo reclamodto = reclamodao.findTopByCorrelativo(interrupcion.getReclamos().get(i).getCorrelativo());
                    reclamodto.setInterrupcion(interrupcion);

                    //persistimos el reclamo para actualizar su informacion
                    reclamodao.save(reclamodto);
                }
            }

            return interrupcion_dto;
        }catch(NoResultException e) {
            System.out.println("ERROR AL FINALIZAR INTERRUPCION " + e.getMessage());
        }

        return interrupcion;

    }

    @Override
    public List<Reclamo> obtenerReclamos(List<Long> interrupciones) {
        List<BigDecimal> r = new ArrayList<>();
        List<Reclamo> reclamos = new ArrayList<>();

        for (int i=0; i<interrupciones.size(); i++) {

            try {
                Query q = em.createNativeQuery("SELECT CORRELATIVO FROM ENS_INTERRUPCION_SUMINISTRO WHERE ID_INTERUPCION=:i");
                q.setParameter("i", interrupciones.get(i));
                List<BigDecimal> r_arr = q.getResultList();
                r.addAll(r_arr);

            }catch(NoResultException e) {
                System.out.println(e.getMessage());
            } catch(PersistenceException p) {
                System.out.println(p.getMessage());
            }
        }

        for (int i=0; i<r.size(); i++) {
            Query q = em.createQuery("SELECT r FROM Reclamo r WHERE r.correlativo=:c");
            q.setParameter("c", r.get(i).toString());
            Reclamo r_ = (Reclamo) q.getSingleResult();
            reclamos.add(r_);

        }

        return reclamos;
    }


    // funcion para asociar una interrupcion a los reclamos

    @Override
    public Interrupcion asociarInterrupcionReclamo(Interrupcion interrupcion) {
        Interrupcion i_ = new Interrupcion();
        List<DetalleInterrupcion> detalleinterrupcion = new ArrayList<>();
        detalleinterrupcion = interrupcion.getDetallemaniobra();
        try {
            i_ = interrupciondao.findTopById(interrupcion.getId());
            Long cor = i_.getCorrelativo();
            i_ = interrupcion;
            i_.setEstado("G");
            i_.setCorrelativo(cor);

            // codigo de reposicion
            String correlativo_db = getCorrelativoByTension(interrupcion.getPeriodo());
            Integer correlativo = Integer.parseInt(correlativo_db) + 1;

            i_.setCodigo_interrupcion(i_.getTipo_sistema().substring(0,1) + i_.getElemento().substring(0,1)  + i_.getPeriodo() + "-" + String.valueOf(cor));


            // establecemos el correlativo
            // i_.setCorrelativo(correlativo.longValue());

            // consulta para traer el maximo id de la interrupcion
            Query q = em.createQuery("SELECT MAX(id) FROM Interrupcion");
            Object max_interr = q.getSingleResult();



            // persistimos la interrupcion
            interrupciondao.save(i_);


            // proceso para la generacion de codigos de reposicion

            DetalleManiobra dt_m = detallemaniobradao.findTopById(i_.getDetallemaniobra().get(0).getItinerario());


            // conteo lineas por maniobra
            int conteo_lineas = detallemaniobradao.countByManiobra(dt_m.getManiobra());

            //recorremos un for para guardar los detalles de la interrupcion
            if(interrupcion.getDetallemaniobra().size() > 0) {
                correlativo = 1;
                for(Integer i=0; i<i_.getDetallemaniobra().size(); i++) {

                    DetalleInterrupcion detalle = new DetalleInterrupcion();
                    detalle = i_.getDetallemaniobra().get(i);
                    detalle.setInterrupcion_(i_);

                    if(dt_m.getGenera_reposicion() == 1) {
                        detalle.setCodigo_reposicion(interrupcion.getCodigo_interrupcion() + '-' + correlativo);
                    }
                    // persistimos el detalle de la interrupcion
                    detalleinterrupciondao.save(detalle);

                    // actualizamos la linea de itinerario
                    DetalleManiobra dtmaniobra = new DetalleManiobra();
                    dtmaniobra = detallemaniobradao.findTopById(i_.getDetallemaniobra().get(i).getItinerario());
                    dtmaniobra.setAsociada(1);

                    detallemaniobradao.save(dtmaniobra);

                    correlativo+=1;

                }

                // conteo de asociaciones por maniobra
                // Integer conteo_asociadas = detallemaniobradao.countByManiobraAndAsociada(dt_m.getManiobra(), 1);
                Query qw = em.createNativeQuery("SELECT COUNT(dt.id) FROM SGT_detalles_maniobra dt WHERE dt.maniobra_id=:m AND asociada=1");
                qw.setParameter("m", dt_m.getManiobra().getId());
                Object o = qw.getSingleResult();

                System.out.println(conteo_lineas + " ** " + o);

                if(conteo_lineas == (Integer) o) {
                    Maniobra m = maniobradao.getManiobraById(dt_m.getManiobra().getId());
                    m.setEstado(3);
                    maniobradao.save(m);
                }
            }



            // recorremos con un for el arreglo de reclamos si la interrupcion es de tipo asociacion
            if(interrupcion.getReclamos().size() > 0) {
                for (Integer i = 0; i < interrupcion.getReclamos().size(); i++) {
                    Reclamo reclamodto = reclamodao.findTopByCorrelativo(interrupcion.getReclamos().get(i).getCorrelativo());
                    reclamodto.setInterrupcion(interrupcion);

                    //persistimos el reclamo para actualizar su informacion
                    reclamodao.save(reclamodto);
                }
            }



        }catch(PersistenceException p) {
            System.out.println(p.getMessage());
        }


        return i_;

    }


    // funcion para desasociar un reclamo

    @Override
    @Transactional
    public String desasociarReclamo(Reclamo reclamo) {

        int actualizacion = 0;

        Long interrup = this.interrupcion_global.getId();

        try {
            Query q = em.createQuery("UPDATE InterrupcionReclamo SET id_interupcion=:i WHERE correlativo=:r");
            q.setParameter("i", interrup.toString());
            q.setParameter("r", reclamo.getCorrelativo());

            // persistimos
            actualizacion = q.executeUpdate();

            if(actualizacion > 0) {
                return "Reclamo desasociado";
            }


        }catch (NoResultException e) {
            System.out.println(e.getMessage());
            return "Error";
        }

        return " ";

    }
}
