package com.edesal.sgt.services;

import com.edesal.sgt.dao.DetalleEnergizacionDAO;
import com.edesal.sgt.dao.EnergizacionDAO;
import com.edesal.sgt.models.DetallesEnergizacion;
import com.edesal.sgt.models.Energizacion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by DHernandez on 15/10/2019.
 */
@Service
public class EnergizacionServiceImpl implements EnergizacionService {

    @Autowired
    EnergizacionService energizacionservice;

    @Autowired
    DetalleEnergizacionDAO detalleenergizaciondao;


    @Autowired
    EnergizacionDAO energizaciondao;

    @Override
    public Energizacion saveCortesEnergizados(Energizacion energizacion) {
        Energizacion e = energizaciondao.save(energizacion);

        for(int i=0; i<energizacion.getEnergizaciondt().size(); i++) {
            DetallesEnergizacion dt = new DetallesEnergizacion();
            dt = energizacion.getEnergizaciondt().get(i);
            dt.setEnergizacion(e);
            this.detalleenergizaciondao.save(dt);
        }

        return e;
    }
}
