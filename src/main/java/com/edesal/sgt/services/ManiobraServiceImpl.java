package com.edesal.sgt.services;

import com.edesal.sgt.dao.DetalleManiobraDAO;
import com.edesal.sgt.dao.ManiobraDAO;
import com.edesal.sgt.models.Corte;
import com.edesal.sgt.models.DetalleManiobra;
import com.edesal.sgt.models.Maniobra;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by DHernandez on 23/8/2019.
 */
@Service
public class ManiobraServiceImpl implements ManiobraService {

    @Autowired
    ManiobraDAO maniobradao;

    @Autowired
    DetalleManiobraDAO detallemaniobradao;

    @PersistenceContext
    EntityManager em;

    @Autowired
    CorteServiceImpl corteservice;


    @Override
    public List<Maniobra> getManiobras() {
        Query q = em.createQuery("SELECT m FROM Maniobra m ORDER BY m.id DESC");
        List<Maniobra> maniobras = q.getResultList();

        return maniobras;

    }

    @Override
    public Maniobra saveManiobra(Maniobra maniobra) {

        Maniobra maniobradto = maniobra;
        maniobradao.save(maniobradto);

        for (Integer i=0; i<maniobradto.getDetallesmaniobra().size(); i++){

            Corte cortedto = new Corte();


            DetalleManiobra detallemaniobra = new DetalleManiobra();
            detallemaniobra = maniobra.getDetallesmaniobra().get(i);
            detallemaniobra.setManiobra(maniobradto);

            String inicial = detallemaniobra.getElemento_aperturado();

            BigDecimal codigo = corteservice.getCorteByCodigo(detallemaniobra.getElemento_aperturado());

            System.out.println(inicial);
            System.out.println(codigo);
            System.out.println(inicial.substring(0,1));


            if(inicial.substring(0,1) == "T"){
                detallemaniobra.setTrafo(codigo);
            } else if(inicial.substring(0,1)=="C"){
                detallemaniobra.setElemento_id(codigo);
            }

            //persistimos los detalles
            detallemaniobradao.save(detallemaniobra);
        }


        return maniobradto;
    }

    @Override
    public Maniobra validarManiobra(Maniobra maniobra)  {

        maniobra.setEstado(2);
        Maniobra maniobradto = maniobradao.save(maniobra);

        return maniobradto;
    }

    @Override
    @Transactional
    public Integer setManiobraAsociada(Long id) {

       /*Query q = em.createNativeQuery("UPDATE SGT_maniobras set estado=3 WHERE id=:id");
       q.setParameter("id",id);
       int m = q.executeUpdate();*/

       return 1;
    }
    @Override
    public String elementoRed(String padre, String hijo) {
        Query q = em.createNativeQuery("WITH redelectrica AS\n" +
                "                        (\n" +
                "                          SELECT TOP 1\n" +
                "                            c.CODIGO_CORTE,\n" +
                "                            c.SEC_CORTE\n" +
                "                          FROM ENS_CORTES c\n" +
                "\n" +
                "                          WHERE c.CODIGO_CORTE =:padre\n" +
                "\n" +
                "\n" +
                "                          UNION ALL\n" +
                "\n" +
                "                          SELECT TOP 100 PERCENT\n" +
                "                            c1.CODIGO_CORTE,\n" +
                "                            c1.SEC_CORTE\n" +
                "                          FROM ENS_CORTES c1\n" +
                "                          INNER JOIN redelectrica AS red ON red.SEC_CORTE = c1.corte_padre\n" +
                "                          WHERE c1.CODIGO_CORTE <>:padre\n" +
                "                        )\n" +
                "\n" +
                "                        SELECT TOP 100 PERCENT\n" +
                "                          cc.CODIGO_CORTE\n" +
                "\n" +
                "\n" +
                "                        FROM redelectrica cc\n" +
                "                        WHERE cc.CODIGO_CORTE =:hijo\n" +
                "                        ORDER BY cc.CODIGO_CORTE");
        q.setParameter("padre", padre);
        q.setParameter("hijo", hijo);

        String elemento = (String) q.getSingleResult();

        return elemento;
    }

    // editar una linea de itinerario
    @Override
    public DetalleManiobra updateItinerario(DetalleManiobra itinerario) {

        // buscamos la linea a editar
        DetalleManiobra linea_dto = detallemaniobradao.findTopById(itinerario.getId());
        itinerario.setManiobra(linea_dto.getManiobra());
        try{

            linea_dto = itinerario;


            // persistimos la actualizacion
            detallemaniobradao.save(linea_dto);

        }catch(NoResultException e) {

            System.out.println("Error al actualizar itinerario " + e.getMessage());

        }

        return linea_dto;

    }
}
