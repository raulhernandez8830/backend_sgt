package com.edesal.sgt.services;

import com.edesal.sgt.models.*;
import com.edesal.sgt.models.SIGET.CompensacionSIGET;
import com.edesal.sgt.models.SIGET.DatosCentroSIGET;
import com.edesal.sgt.models.SIGET.InstalacionSIGET;
import com.edesal.sgt.models.SIGET.ReclamoInterSIGET;

import java.util.List;

/**
 * Created by DHernandez on 21/8/2019.
 */
public interface InterrupcionService {

    // listar las interrupciones almacenadas en la db
    public List<Interrupcion> getInterrupciones();


    //listar elementos filtrados por sistema
    public List<Object> getElementosBySistema(String sistema);

    //listar causas de interrupcion
    public List<Causa> getCausasInterrupcion();

    //guardar una interrupcion
    public Interrupcion saveInterrupcion(Interrupcion interrupcion);

    // obtener el conteo de las interrupciones generadas por periodo segun tension para generar correlativo
    String getCorrelativoByTension(String periodo);

    // obtener las interrupciones cerradas
    List<Interrupcion> interrupcionesCerradas();

    public List<UsuarioAfectado> getUsuariosAfectados(String periodo);

    // obtener causa por medio de su nombre
    public Causa getCausa(String nombre);

    // metodo para actualizar una interrupcion
    public Interrupcion updateInterrupcion(Interrupcion interrupcion);

    // metodo para listar interrupciones segun reclamos seleccionados en el frontend
    public Interrupcion getInterrupcionByReclamo();


    // obtener el conteo de los suministros
    public Integer getConteoNIS();

    // obtener el conteo de los trafos afectados
    public Integer getConteoTrafos();

    // obtener el conteo agrupado de los suministros afectados por interrupcion
    public List<InterrupcionSuministro> getSuministrosAfectadosByInterrup();

    // obtener el conteo agrupado de los trafos afectados por interrupcion
    public List<InterrupcionSuministro> getTrafoAfectadosByInterrup();

    // obtener la data de la tabla de interrupciones para la SIGET
    public List<InterrupcionSIGET> tbl_interrupcionesSIGET(String periodo);

    // obtener la data de la tabla de reposiciones para la SIGET
    public List<ReposicionSIGET> tbl_reposicionesSIGET(String periodo);

    // obtener la data de la tabla de interrupciones externas para la SIGET
    public List<InterrupcionExternaSIGET> tbl_externasSIGET(String periodo);

    // obtener la data de la tabla de CENTROS MTBT para la SIGET
    public List<CentroMTBT> tbl_centrosmtbt(String periodo);

    // obtener la data de la tabla de REP_USUARIOS
    public List<RepUsuarioSIGET> tbl_repusuarios(String periodo);

    // obtener la data de la tabla de RECLAMOS_INTERR
    public List<ReclamoInterSIGET> tbl_reclamosinterr(String periodo);

    // obtener la data de la tabla de DATOS CENTROS
    public List<DatosCentroSIGET> tbl_datoscentros(String periodo);


    // obtener la data de la tabla de INSTALACIONES
    public List<InstalacionSIGET> tbl_instalacionessiget(String periodo);

    // obtener la data de la tabla de INSTALACIONES
    public List<CompensacionSIGET> tbl_compensacionessiget(String periodo);


    // finalizar una interrupcion
    public Interrupcion finalizarInterrupcion(Interrupcion interrupcion);

    // obtener reclamos
    public List<Reclamo> obtenerReclamos(List<Long> interrupciones);


    // funcion para la asociacion de interrupciones y reclamos
    public Interrupcion asociarInterrupcionReclamo(Interrupcion interrupcion);

    // funcion para desasociar un reclamo
    public String desasociarReclamo(Reclamo reclamo);


}
