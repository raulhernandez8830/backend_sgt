package com.edesal.sgt.services;

import com.edesal.sgt.models.Energizacion;
import com.edesal.sgt.models.Energizado;

import java.util.List;

/**
 * Created by DHernandez on 15/10/2019.
 */
public interface EnergizacionService {

    // guardar cortes energizados
    public Energizacion saveCortesEnergizados(Energizacion energizacion);
}
