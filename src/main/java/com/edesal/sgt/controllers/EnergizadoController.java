package com.edesal.sgt.controllers;

import com.edesal.sgt.models.Energizado;
import com.edesal.sgt.services.EnergizadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;


@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})//@CrossOrigin(origins = {"http://localhost"})
@RequestMapping("/api")
public class EnergizadoController {

    @Autowired
    EnergizadoService energizadoservice;


    @RequestMapping(value = "/configuracionenergizados/{aperturado}/{cerrado}", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public Energizado getEnergizadosByConfiguracion(@PathVariable("aperturado") String aperturado, @PathVariable("cerrado") String cerrado) {
        Energizado configuracionenergizados = energizadoservice.getEnergizadosByConfiguracion(aperturado,cerrado);
        
        return configuracionenergizados;
    }
}
