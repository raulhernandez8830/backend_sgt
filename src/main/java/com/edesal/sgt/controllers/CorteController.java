package com.edesal.sgt.controllers;

import com.edesal.sgt.models.Corte;
import com.edesal.sgt.models.RedElectrica;
import com.edesal.sgt.services.CorteServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by DHernandez on 4/9/2019.
 */

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})//@CrossOrigin(origins = {"http://localhost"})
@RequestMapping("/api")
public class CorteController {


    @Autowired
    CorteServiceImpl corteservice;


    // metodo para mostrar un corte por medio de su codigo
    /*
    @RequestMapping(path = "/cortebycodigo/{codigo}", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public Corte getCorteByCodigo(@PathVariable("codigo") String codigo){

        Corte cortedto = corteservice.getCorteByCodigo(codigo);

        return cortedto;
    }*/


    // metodo para obtener todos los tipos de elementos a nivel de red electrica
    @RequestMapping(value = "/elementos", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public List<String> getElementos() {
        List<String> elementos = corteservice.getAllElementos();

        return elementos;
    }

    @RequestMapping(value = "/elementosbycircuito/{trafo}", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public List<String> getElementos(@PathVariable("trafo") String trafo) {
        List<String> elementos = corteservice.getElementosByCircuito(trafo);
        return elementos;
    }


    //metodo para obtener la red electrica de EDESAL
    @RequestMapping(value="/redelectrica", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public List<RedElectrica> getRedElectrica() {
        List<RedElectrica> redelectrica = corteservice.getRedElectrica();

        return redelectrica;
    }

    // metodo para obtener los cortes que pertenecen a maniobras de transferencias para elementos aperturados
    @RequestMapping(value = "/cortesmtap", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public List<String> getElementosMT() {
        List<String> cortes = corteservice.getElementosAperturadosMT();

        return cortes;
    }

    // buscar un elemento para verificar sus banderas activas mt
    // metodo para mostrar un corte por medio de su codigo
    @RequestMapping(path = "/buscarelementoaperturado/{elemento}", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public Corte buscarElementoAperturado(@PathVariable("elemento") String elemento) {
        Corte corte = corteservice.buscarElementoAperturado(elemento);

        return corte;
    }

    @GetMapping("/cortes")
    public List<Corte> getCortes() {
        List<Corte> cortes = corteservice.getCortes();
        return cortes;
    }


    // obtener un corte por su id
    @RequestMapping(path = "/corte/{id}", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public Corte getCorteById(@PathVariable("id") String id) {
        Corte corte = corteservice.getCorteById(id);

        return corte;
    }


    @RequestMapping(value = "/cortebycodigo/{cod}", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public Corte getCorteByCodigo(@PathVariable String cod) {
        Corte corte = corteservice.obtenerCorteXCodigo(cod);

        return corte;
    }



}
