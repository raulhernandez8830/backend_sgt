package com.edesal.sgt.controllers;

import com.edesal.sgt.models.Reclamo;
import com.edesal.sgt.services.ReclamoServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by DHernandez on 17/9/2019.
 */
@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})//@CrossOrigin(origins = {"http://localhost"})
@RequestMapping("/api")
public class ReclamoController {


    @Autowired
    ReclamoServiceImpl reclamoservice;


    @RequestMapping(value = "/reclamos", method= RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public List<Reclamo> getReclamos() {
        List<Reclamo> reclamos = reclamoservice.getReclamos();

        return reclamos;
    }
}
