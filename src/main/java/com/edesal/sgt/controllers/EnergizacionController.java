package com.edesal.sgt.controllers;

import com.edesal.sgt.models.Energizacion;
import com.edesal.sgt.services.EnergizacionServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by DHernandez on 15/10/2019.
 */
@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})//@CrossOrigin(origins = {"http://localhost"})
@RequestMapping("/api")
public class EnergizacionController {

    @Autowired
    EnergizacionServiceImpl energizacionservice;


    @RequestMapping(value = "/saveenergizacion", method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public Energizacion  saveEnergizacion(@RequestBody Energizacion energizacion) {
        Energizacion ener = energizacionservice.saveCortesEnergizados(energizacion);

        return ener;
    }


}
