package com.edesal.sgt.controllers;

import com.edesal.sgt.models.*;
import com.edesal.sgt.models.SIGET.CompensacionSIGET;
import com.edesal.sgt.models.SIGET.DatosCentroSIGET;
import com.edesal.sgt.models.SIGET.InstalacionSIGET;
import com.edesal.sgt.models.SIGET.ReclamoInterSIGET;
import com.edesal.sgt.services.InterrupcionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by DHernandez on 21/8/2019.
 */
@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})//@CrossOrigin(origins = {"http://localhost"})
@RequestMapping("/api")
public class InterrupcionController {

    @Autowired
    InterrupcionService interrupcionservice;


    // ws para obtener las interrupciones almacenadas en la db
    @RequestMapping(value = "/interrupciones", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public List<Interrupcion> getInterrupciones(){

        List<Interrupcion> interrupciones = interrupcionservice.getInterrupciones();

        return interrupciones;
    }

    // ws para obtener las interrupciones almacenadas en la db
    @RequestMapping(value = "/interrupcionesCerradas", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public List<Interrupcion> getInterrupcionesCerradas(){

        List<Interrupcion> interrupciones = interrupcionservice.interrupcionesCerradas();

        return interrupciones;
    }


    //ws para obtener los elementos filtrados por sistema
    @RequestMapping(path = "/elementosbysistema/{sistema}")
    public List<Object> getElementosBySistema(@PathVariable("sistema") String sistema) {
        List<Object> elementos = this.interrupcionservice.getElementosBySistema(sistema);
        return elementos;
    }

    // ws para actualizar una interrupcion
    @RequestMapping(value = "/updateinterrupcion", method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public Interrupcion updateInterrupcion(@RequestBody Interrupcion interrupcion) {
        Interrupcion i = interrupcionservice.updateInterrupcion(interrupcion);


        return i;
    }
    
    // obtener las causas de interrupcion
    @RequestMapping(value = "/causas", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public List<Causa> getCausasInterrupcion() {
        List<Causa> causas = this.interrupcionservice.getCausasInterrupcion();

        return causas;
    }


    @RequestMapping(value="/interrupcion",method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public Interrupcion saveInterrupcion(@RequestBody Interrupcion interrupcion) {



        Interrupcion interrupciondto = interrupcionservice.saveInterrupcion(interrupcion);
        return interrupciondto;
    }


    @RequestMapping(value = "/usuariosafectados/{periodo}", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public List<UsuarioAfectado> generarUsuariosAfectados(@PathVariable String periodo) {
        List<UsuarioAfectado> usuariosafectados = interrupcionservice.getUsuariosAfectados(periodo);

        return usuariosafectados;
    }


    // ws para listar las interrupciones ligadas a reclamos
    @RequestMapping(value = "/interrupcionesxreclamo", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public Interrupcion  getInterrupcionesXReclamo() {
        Interrupcion i = interrupcionservice.getInterrupcionByReclamo();

        return i;
    }


    @RequestMapping(value="/conteoNIS", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public Integer getConteoNIS() {
        Integer conteos = interrupcionservice.getConteoNIS();

        return conteos;
    }


    @RequestMapping(value="/conteoTrafos", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public Integer getConteoTrafos() {
        Integer conteos = interrupcionservice.getConteoTrafos();

        return conteos;
    }


    // conteo agrupado de suministros afectados por interrupcion
    @RequestMapping(value="/conteoagrupadointerrupNIS", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public List<InterrupcionSuministro> getConteoAgrupadoInterrupNIS() {
        List<InterrupcionSuministro> conteo = interrupcionservice.getSuministrosAfectadosByInterrup();
        return conteo;
    }


    // conteo agrupado de trafos afectados por interrupcion
    @RequestMapping(value="/conteoagrupadointerrupTRAFO", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public List<InterrupcionSuministro> getConteoAgrupadoInterrupTrafo() {
        List<InterrupcionSuministro> conteo = interrupcionservice.getTrafoAfectadosByInterrup();
        return conteo;
    }


    // ws para generar la tabla de interrupciones SIGET
    @RequestMapping(value = "/interrupcionessiget/{periodo}", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public List<InterrupcionSIGET> tbl_interrupcionesSIGET(@PathVariable("periodo") String periodo){
        List<InterrupcionSIGET> interrupciones = interrupcionservice.tbl_interrupcionesSIGET(periodo);
        return interrupciones;
    }


    // WS para generar la tabla de reposiciones SIGET
    @RequestMapping(value = "/reposicionessiget/{periodo}", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public List<ReposicionSIGET> tbl_reposicionesSIGET(@PathVariable("periodo") String periodo){
        List<ReposicionSIGET> reposiciones = interrupcionservice.tbl_reposicionesSIGET(periodo);
        return reposiciones;
    }



    // WS para generar la tabla de interrupciones externas SIGET
    @RequestMapping(value = "/externassiget/{periodo}", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public List<InterrupcionExternaSIGET> tbl_externasSIGET(@PathVariable("periodo") String periodo){
        List<InterrupcionExternaSIGET> externas = interrupcionservice.tbl_externasSIGET(periodo);
        return externas;
    }


    // WS para generar la tabla de centros mt bt SIGET
    @RequestMapping(value = "/centrosmtbtsiget/{periodo}", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public List<CentroMTBT> tbl_centrosmtbt(@PathVariable("periodo") String periodo){
        List<CentroMTBT> centrosmtbt = interrupcionservice.tbl_centrosmtbt(periodo);
        return centrosmtbt;
    }

    // WS para generar la tabla de RepUsuarios  SIGET
    @RequestMapping(value = "/repusuariossiget/{periodo}", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public List<RepUsuarioSIGET> tbl_repusuarios(@PathVariable("periodo") String periodo){
        List<RepUsuarioSIGET> u = interrupcionservice.tbl_repusuarios(periodo);
        return u;
    }


    // WS para generar la tabla de RepUsuarios  SIGET
    @RequestMapping(value = "/reclamosinterr/{periodo}", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public List<ReclamoInterSIGET> tbl_reclamointer(@PathVariable("periodo") String periodo){
        List<ReclamoInterSIGET> R = interrupcionservice.tbl_reclamosinterr(periodo);
        return R;
    }


    // WS para generar la tabla de RepUsuarios  SIGET
    @RequestMapping(value = "/datoscentro/{periodo}", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public List<DatosCentroSIGET> tbl_datoscentros(@PathVariable("periodo") String periodo){
        List<DatosCentroSIGET> r = interrupcionservice.tbl_datoscentros(periodo);
        return r;
    }

    // WS para generar la tabla de RepUsuarios  SIGET
    @RequestMapping(value = "/instalaciones/{periodo}", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public List<InstalacionSIGET> tbl_instalaciones(@PathVariable("periodo") String periodo){
        List<InstalacionSIGET> r = interrupcionservice.tbl_instalacionessiget(periodo);
        return r;
    }

    // WS para generar la tabla de RepUsuarios  SIGET
    @RequestMapping(value = "/compensaciones/{periodo}", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public List<CompensacionSIGET> tbl_compensacionessiget(@PathVariable("periodo") String periodo){
        List<CompensacionSIGET> r = interrupcionservice.tbl_compensacionessiget(periodo);
        return r;
    }


    @RequestMapping(value="/finalizarinterrupcion", method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public Interrupcion finalizarInterrupcion(@RequestBody Interrupcion interrupcion) {
      Interrupcion interrupciondto =  interrupcionservice.finalizarInterrupcion(interrupcion);
      return interrupciondto;
    }

    // buscar reclamos de interrupciones
    @RequestMapping(value="/obtenerreclamos", method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public List<Reclamo> obtenerReclamos(@RequestBody List<Long> interrupciones) {
        List<Reclamo> reclamos = interrupcionservice.obtenerReclamos(interrupciones);
        return reclamos;
    }

    @RequestMapping(value="/asocinterrupcionreclamo", method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public Interrupcion asociarInterrupcionReclamo(@RequestBody Interrupcion interrupcion) {
        Interrupcion in = interrupcionservice.asociarInterrupcionReclamo(interrupcion);
        return in;
    }


    // ws para desasociar un reclamo y crear una nueva interrupcion
    @RequestMapping(value="/desasociarreclamo", method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public String desasociarReclamo(@RequestBody Reclamo reclamo) {
        String mensaje = interrupcionservice.desasociarReclamo(reclamo);
        return mensaje;
    }
}
