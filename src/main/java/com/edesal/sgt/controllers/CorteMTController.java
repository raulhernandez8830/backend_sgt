package com.edesal.sgt.controllers;

import com.edesal.sgt.services.CorteMTServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by DHernandez on 1/10/2019.
 */
@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})//@CrossOrigin(origins = {"http://localhost"})
@RequestMapping("/api")
public class CorteMTController {

    @Autowired
    CorteMTServiceImpl cortemtservice;

    @RequestMapping(value = "/elementoscierremt/{elemento}/{opcion}", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public List<String> getElementosCierrePS(@PathVariable("elemento") String elemento, @PathVariable("opcion") String opcion ) {
        List<String> cortes = cortemtservice.getElementosCierrePS(elemento, opcion);
        return cortes;
    }

    // obtener los elementos de tipo principal
    @RequestMapping(value = "/elementosprincipales/{corte}/{opcion}", method =  RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public List<String> getElementosPrincipales(@PathVariable("corte") String corte, @PathVariable("opcion") String opcion) {
        List<String> cortes = cortemtservice.getElementosPrincipales(corte,opcion);

        return cortes;
    }
}
