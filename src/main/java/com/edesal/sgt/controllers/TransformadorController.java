package com.edesal.sgt.controllers;

import com.edesal.sgt.models.*;
import com.edesal.sgt.services.TransformadorServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by DHernandez on 15/5/2020.
 */
@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})//@CrossOrigin(origins = {"http://localhost"})
@RequestMapping("/api")
public class TransformadorController {

    @Autowired
    TransformadorServiceImpl transformadorservice;

    @GetMapping("/transformadores")
    public List<Transformador> getTrafos() {
        List<Transformador> transformadores = this.transformadorservice.getTrafos();

        return transformadores;
    }

    // obtener la informacion del poste ligado al trafo
    @RequestMapping(path = "/poste/{poste}", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public  List<Poste> getPosteByTrafo(@PathVariable String poste) {
        List<Poste> postedto = transformadorservice.getPosteByTrafo(poste);

        return postedto;
    }

    // obtener la informacion del circuito ligado al trafo
    @RequestMapping(path = "/circuito/{circuito}", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public Circuito getCircuitoByTrafo(@PathVariable String circuito) {
        Circuito circuitodto = transformadorservice.getCircuitoByTrafo(circuito);

        return circuitodto;
    }

    // obtener los circuitos listados en la db
    @GetMapping("/circuitos")
    public List<Circuito> getAllCircuitos() {
      List<Circuito> circuitos = transformadorservice.getAllCircuitos();

      return circuitos;
    }

    @RequestMapping(path = "/savetrafo", method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public Transformador saveTrafo(@RequestBody Transformador trafo) {
        Transformador trafodto = transformadorservice.saveTrafo(trafo);
        return trafodto;
    }

    @GetMapping("/postes")
    public List<String> getPostes() {
        List<String> postes = transformadorservice.getPostes();
        return postes;
    }

    @GetMapping("/voltajes")
    public List<Voltaje> getVoltajes() {
        List<Voltaje> voltajes = transformadorservice.getVoltajes();
        return voltajes;
    }

    @GetMapping("/estructurasmateriales")
    public List<MaterialesEstructura> getEstructurasMateriales() {
        List<MaterialesEstructura> estructuras = transformadorservice.getEsctructurasMateriales();
        return estructuras;
    }

    @RequestMapping(path = "/updatetrafo", method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public Transformador updatetrafo(@RequestBody Transformador trafo) {
        Transformador trafodto = transformadorservice.update(trafo);
        return trafo;
    }

    @RequestMapping(path = "/bajatrafo", method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public Transformador bajaTrafo(@RequestBody Transformador trafo) {
        Transformador trafodto = transformadorservice.bajaTrafo(trafo);
        return trafo;
    }


    // obtener los suministros activos de un transformador
    @GetMapping("/getsuministrostrafo/{codigo}")
    public List<BigDecimal> getSuministrosTrafo(@PathVariable String codigo) {
        List<BigDecimal> suministros = transformadorservice.suministrosActivosTrafo(codigo);
        return suministros;
    }
}
