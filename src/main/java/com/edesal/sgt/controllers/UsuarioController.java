package com.edesal.sgt.controllers;

import com.edesal.sgt.models.Usuario;
import com.edesal.sgt.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * Created by DHernandez on 19/8/2019.
 */
@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})//@CrossOrigin(origins = {"http://localhost"})
@RequestMapping("/api")
public class UsuarioController {

    @Autowired
    UsuarioService usuarioservice;


    // obtener los usuarios almacendos en la db
    @GetMapping("/usuarios")
    public List<Usuario> getUsuarios(){
        List<Usuario> usuarios = usuarioservice.getUsuarios();

        return usuarios;
    }

    //validar credenciales del usuario que intenta hacer login
    @RequestMapping(value = "/usuario",method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public Usuario validarCrendenciales(@RequestBody Usuario usuario){
        Usuario usuariodto = usuarioservice.validarCredenciales(usuario);

        return usuariodto;
    }


}
