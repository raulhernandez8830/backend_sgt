package com.edesal.sgt.controllers;

import com.edesal.sgt.models.DetalleManiobra;
import com.edesal.sgt.models.Maniobra;
import com.edesal.sgt.services.ManiobraServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by DHernandez on 23/8/2019.
 */
@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})//@CrossOrigin(origins = {"http://localhost"})
@RequestMapping("/api")
public class ManiobraController {

    @Autowired
    ManiobraServiceImpl maniobraservice;



    @GetMapping("/maniobras")
    public List<Maniobra> getManiobras(){
        List<Maniobra> maniobras = maniobraservice.getManiobras();

        return maniobras;
    }

    // ws para guardar una maniobra
    @RequestMapping(value = "/savemaniobra", method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public Maniobra  saveManiobra(@RequestBody Maniobra maniobra){
        Maniobra maniobradto = maniobraservice.saveManiobra(maniobra);

        return maniobradto;
    }

    @RequestMapping(value = "/validarmaniobra", method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public String validarManiobra(@RequestBody Maniobra maniobra ) {
        Maniobra maniobradto  = maniobraservice.validarManiobra(maniobra);

        if (maniobradto != null) {
            return "true";
        } else {
            return "false";
        }

    }

    @RequestMapping(value ="/estadoasociada/{id}", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public Integer setEstadoAsociada(@PathVariable("id") Long id){
        int update = maniobraservice.setManiobraAsociada(id);

        return update;
    }

    @GetMapping("/comprobarelementored/{padre}/{hijo}")
    public String elemento(@PathVariable("padre") String padre, @PathVariable("hijo") String hijo) {
        String elemento = maniobraservice.elementoRed(padre,hijo);
        return elemento;
    }


    // actualizamos linea de maniobra
    @RequestMapping(value="/actualizaritinerario", method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public DetalleManiobra actualizarItinerario(@RequestBody DetalleManiobra itinerario) {
        DetalleManiobra linea_dto = maniobraservice.updateItinerario(itinerario);
        return linea_dto;
    }

}
