package com.edesal.sgt.controllers;

import com.edesal.sgt.models.TipoTransferencia;
import com.edesal.sgt.models.Transferencia;
import com.edesal.sgt.services.TransferenciaServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by DHernandez on 5/9/2019.
 */
@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})//@CrossOrigin(origins = {"http://localhost"})
@RequestMapping("/api")
public class TransferenciaController {

    @Autowired
    TransferenciaServiceImpl transferenciaservice;


    @RequestMapping(value = "transferencia", method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public Transferencia saveTransferencia(@RequestBody Transferencia transferencia) {
        Transferencia transferenciadto = transferenciaservice.saveTransferencia(transferencia);
        return transferenciadto;
    }


    // WS obtener los tipos de transferencias
    @RequestMapping(path = "/tipostransferencia", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public List<TipoTransferencia> getTipoTransferencia() {

        List<TipoTransferencia> tipos = transferenciaservice.getTipoTransferencias();

        return tipos;
    }

    // WS para obtener un tipo de transferencia por su id
    @RequestMapping(path = "/transferencia/{id}", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public TipoTransferencia getTransferencia(@PathVariable Long id) {
        TipoTransferencia transferencia = transferenciaservice.getTrasnferencia(id);

        return transferencia;
    }

}
